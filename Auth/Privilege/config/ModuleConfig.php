<?php

declare(strict_types=1);

namespace Paneric\Auth\Privilege\config;

use Paneric\Auth\Privilege\Model\PrivilegeVLD;
use Paneric\Auth\Privilege\Model\PrivilegeDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'privilege';
    protected string $prefix = 'prv_';

    protected string $uniqueKey = 'ref';

    protected bool $stringId = true;

    protected string $daoClass = PrivilegeDAO::class;
    protected string $vldClass = PrivilegeVLD::class;
}

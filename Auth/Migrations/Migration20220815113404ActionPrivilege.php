<?php

declare(strict_types=1);

namespace Paneric\Auth\Migrations;

use Exception;
use Paneric\Migrations\AbstractMigration;

final class Migration20220815113404ActionPrivilege extends AbstractMigration
{
    private const SQL_PATH = 'Auth/script/sql/action-privilege-up.sql';

    public function getDescription(): string
    {
        return '';
    }

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        $this->executeQuery(file_get_contents(self::SQL_PATH));
    }
}

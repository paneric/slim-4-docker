<?php

declare(strict_types=1);

namespace Paneric\Auth\Credential\Controller;

use Paneric\ComponentModule\Controller\ApiModuleController;
use Paneric\ComponentModule\Interfaces\Action\CreateApiActionInterface;
use Paneric\ComponentModule\Interfaces\ActionListInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiCredentialController extends ApiModuleController
{
    public function register(
        Request $request,
        Response $response,
        CreateApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }
}

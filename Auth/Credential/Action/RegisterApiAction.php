<?php

declare(strict_types=1);

namespace Paneric\Auth\Credential\Action;

use Exception;
use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Interfaces\Action\CreateApiActionInterface;
use Paneric\ComponentModule\Interfaces\GuardInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;

class RegisterApiAction extends Action implements CreateApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected GuardInterface $guard;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        GuardInterface $guard
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->create();
        $this->guard = $guard;
    }

    public function __invoke(Request $request): ?array
    {
        $attributes = $request->getParsedBody();

        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $createUniqueCriteria = $this->config['create_unique_criteria'];

        try {
            $this->status = 201;

            new $this->config['vld_class']($dto);

            if ($this->config['string_id']) {
                $dto->setId($this->guard->setUniqueId());
            }

            $dto->setPasswordHash(
                $this->guard->hashPassword($dto->getPasswordHash())
            );
            $dto->setActivationToken(
                $this->guard->generateRandomString()
            );

            try {
                $createResult = $this->adapter->createUnique($createUniqueCriteria($attributes), $dto);

                if ($createResult === null) {
                    throw new Exception('db_add_unique_error');
                }

                return [
                    'status' => $this->status
                ];
            } catch (Throwable $e) {
                $this->status = 400;

                return [
                    'status' => $this->status,
                    'error' => $e->getMessage(),
                    'body' => $attributes
                ];
            }
        } catch (ValidationException $e) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => $e->getReport(),
                'body' => $attributes
            ];
        }
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}

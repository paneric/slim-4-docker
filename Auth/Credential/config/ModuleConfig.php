<?php

declare(strict_types=1);

namespace Paneric\Auth\Credential\config;

use Paneric\Auth\Credential\Model\CredentialVLD;
use Paneric\Auth\Credential\Model\CredentialDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'credential';
    protected string $prefix = 'crd_';

    protected string $uniqueKey = 'gsm';//unique key

    protected bool $stringId = true;

    protected string $daoClass = CredentialDAO::class;
    protected string $vldClass = CredentialVLD::class;
}

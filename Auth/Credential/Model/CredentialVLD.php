<?php

declare(strict_types=1);

namespace Paneric\Auth\Credential\Model;

use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Interfaces\ValidatorInterface;
use Paneric\ComponentModule\Model\Interfaces\DataObjectInterface;

class CredentialVLD implements ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function invoke(DataObjectInterface|array $dao): void
    {
        throw new ValidationException();
    }
}

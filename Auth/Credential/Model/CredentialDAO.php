<?php

declare(strict_types=1);

namespace Paneric\Auth\Credential\Model;

use Paneric\DataObject\DAO;

class CredentialDAO extends DAO
{
    use CredentialTrait;

    protected ?string $id;

    protected string $email;
    protected string $gsm;
    protected string $passwordHash;
    protected ?string $apiToken;
    protected int $terms = 0;
    protected int $isActive = 0;
    protected ?string $activationToken;
    protected ?string $resetToken;

    public function __construct()
    {
        $this->prefix = 'crd_';

        $this->setMaps();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
    public function getGsm(): string
    {
        return $this->gsm;
    }
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }
    public function getApiToken(): string
    {
        return $this->apiToken;
    }
    public function getTerms(): int
    {
        return $this->terms;
    }
    public function getIsActive(): int
    {
        return $this->isActive;
    }
    public function getActivationToken(): string
    {
        return $this->activationToken;
    }
    public function getResetToken(): string
    {
        return $this->resetToken;
    }


    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    public function setGsm(string $gsm): void
    {
        $this->gsm = $gsm;
    }
    public function setPasswordHash(string $passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }
    public function setApiToken(string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }
    public function setTerms(int $terms): void
    {
        $this->terms = $terms;
    }
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }
    public function setActivationToken(string $activationToken): void
    {
        $this->activationToken = $activationToken;
    }
    public function setResetToken(string $resetToken): void
    {
        $this->resetToken = $resetToken;
    }
}

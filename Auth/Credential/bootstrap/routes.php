<?php

declare(strict_types=1);

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Auth\Credential\Controller\ApiCredentialController;
use Paneric\Auth\Credential\Action\RegisterApiAction;

$module = 'credential';

if (isset($routePrefix)) {
    require ROOT_FOLDER . 'vendor/paneric/component-module/src/bootstrap/' . $routePrefix . '-routes.php';
}

if (isset($slim, $container)) {
    try {
        $slim->post('/api/credential/register', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->create(
                $request,
                $response,
                $this->get(RegisterApiAction::class)
            );
        })->setName('api-' . $module . '.register');
    } catch (Exception $e) {
    }
}

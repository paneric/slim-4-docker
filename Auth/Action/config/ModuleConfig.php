<?php

declare(strict_types=1);

namespace Paneric\Auth\Action\config;

use Paneric\Auth\Action\Model\ActionVLD;
use Paneric\Auth\Action\Model\ActionDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'action';
    protected string $prefix = 'acn_';

    protected array $uniqueKeys = ['ref'];//unique keys

    protected string $daoClass = ActionDAO::class;
    protected string $vldClass = ActionVLD::class;

    protected bool $stringId = true;
}

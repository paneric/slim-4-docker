<?php

declare(strict_types=1);

$module = 'privilege-role';

if (isset($routePrefix)) {
    require ROOT_FOLDER . 'vendor/paneric/component-module/src/bootstrap/' . $routePrefix . '-routes.php';
}

<?php

declare(strict_types=1);

namespace Paneric\Auth\PrivilegeRole\config;

use Paneric\Auth\PrivilegeRole\Model\PrivilegeRoleADAO;
use Paneric\Auth\PrivilegeRole\Model\PrivilegeRoleDAO;
use Paneric\Auth\PrivilegeRole\Model\PrivilegeRoleVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'privilege_role';
    protected string $prefix = 'pvrl_';

    protected bool $stringId = false;

    protected array $uniqueKey = ['privilege_id', 'role_id'];//unique key

    protected string $adaoClass = PrivilegeRoleADAO::class;
    protected string $daoClass = PrivilegeRoleDAO::class;
    protected string $vldClass = PrivilegeRoleVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM privilege_role AS pvrl
                        LEFT JOIN privilege AS prv ON pvrl.pvrl_privilege_id = prv.prv_id
                        LEFT JOIN role AS rle ON pvrl.pvrl_role_id = rle.rle_id
                ";
}

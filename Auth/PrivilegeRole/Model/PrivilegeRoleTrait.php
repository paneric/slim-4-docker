<?php

declare(strict_types=1);

namespace Paneric\Auth\PrivilegeRole\Model;

trait PrivilegeRoleTrait
{
    protected ?string $privilegeId;
    protected ?string $roleId;


    public function getPrivilegeId(): ?string
    {
        return $this->privilegeId;
    }
    public function getRoleId(): ?string
    {
        return $this->roleId;
    }


    public function setPrivilegeId(?string $privilegeId): void
    {
        $this->privilegeId = $privilegeId;
    }
    public function setRoleId(?string $roleId): void
    {
        $this->roleId = $roleId;
    }
}

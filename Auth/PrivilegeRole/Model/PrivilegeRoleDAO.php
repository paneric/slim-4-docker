<?php

declare(strict_types=1);

namespace Paneric\Auth\PrivilegeRole\Model;

use Paneric\DataObject\DAO;

class PrivilegeRoleDAO extends DAO
{
    use PrivilegeRoleTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'pvrl_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(null|int|string $id): void
    {
        $this->id = (int) $id;
    }
}

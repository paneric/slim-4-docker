<?php

declare(strict_types=1);

namespace Paneric\Auth\AcnCrd\Model;

use Paneric\Auth\Action\Model\ActionDAO;
use Paneric\Auth\Credential\Model\CredentialDAO;

class AcnCrd
{
    protected ActionDAO $action;
    protected CredentialDAO $credential;

    public function __construct(array $values = null)
    {
        if ($values) {
            $this->action = new ActionDAO();
            $values = $this->action->hydrate($values);

            $this->credential = new CredentialDAO();
            $values = $this->credential->hydrate($values);

            unset($values);
        }
    }

    public function getAction(): ActionDAO
    {
        return $this->action;
    }
    public function getCredential(): CredentialDAO
    {
        return $this->credential;
    }

    public function setAction(ActionDAO $action): void
    {
        $this->action = $action;
    }
    public function setAcnRef(CredentialDAO $credential): void
    {
        $this->credential = $credential;
    }
}

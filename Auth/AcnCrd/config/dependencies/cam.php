<?php

declare(strict_types=1);

use Paneric\Auth\AcnCrd\config\ModuleConfig;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

return array_merge(
    (array) require ROOT_FOLDER . 'vendor/paneric/component-module/src/config/dependencies/cam.php',
    [
        ModuleConfigInterface::class => static function (): ModuleConfig {
            return new ModuleConfig();
        },
    ]
);

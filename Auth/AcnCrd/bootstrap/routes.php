<?php

declare(strict_types=1);

$module = 'action-credential';

if (isset($routePrefix)) {
    require ROOT_FOLDER . 'vendor/paneric/component-module/src/bootstrap/' . $routePrefix . '-routes.php';
}

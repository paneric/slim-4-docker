<?php

declare(strict_types=1);

namespace Paneric\Auth\ActionPrivilege\config;

use Paneric\Auth\ActionPrivilege\Model\ActionPrivilegeADAO;
use Paneric\Auth\ActionPrivilege\Model\ActionPrivilegeDAO;
use Paneric\Auth\ActionPrivilege\Model\ActionPrivilegeVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'action_privilege';
    protected string $prefix = 'acpv_';

    protected bool $stringId = false;

    protected array $uniqueKeys = ['action_id', 'privilege_id'];//unique key

    protected string $adaoClass = ActionPrivilegeADAO::class;
    protected string $daoClass = ActionPrivilegeDAO::class;
    protected string $vldClass = ActionPrivilegeVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM action_privilege AS mt
                        LEFT JOIN action AS stl ON mt.acpv_action_id = stl.acn_id
                        LEFT JOIN privilege AS str ON mt.acpv_privilege_id = str.prv_id
                ";
}

<?php

declare(strict_types=1);

namespace Paneric\Auth\ActionPrivilege\Model;

trait ActionPrivilegeTrait
{
    protected ?string $actionId;
    protected ?string $privilegeId;


    public function getActionId(): ?string
    {
        return $this->actionId;
    }
    public function getPrivilegeId(): ?string
    {
        return $this->privilegeId;
    }


    public function setActionId(?string $actionId): void
    {
        $this->actionId = $actionId;
    }
    public function setPrivilegeId(?string $privilegeId): void
    {
        $this->privilegeId = $privilegeId;
    }
}

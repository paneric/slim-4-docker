<?php

declare(strict_types=1);

namespace Paneric\Auth\ActionPrivilege\Model;

use Paneric\DataObject\DAO;

class ActionPrivilegeDAO extends DAO
{
    use ActionPrivilegeTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'acpv_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(null|int|string $id): void
    {
        $this->id = (int) $id;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Auth\ActionPrivilege\Model;

use Paneric\Auth\Action\Model\ActionDAO;
use Paneric\Auth\Privilege\Model\PrivilegeDAO;
use Paneric\DataObject\ADAO;

class ActionPrivilegeADAO extends ADAO
{
    use ActionPrivilegeTrait;

    protected ?int $id;

    protected ?ActionDAO $action;
    protected ?PrivilegeDAO $privilege;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'acpv_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->action = new ActionDAO();
            $this->values = $this->action->hydrate($this->values);

            $this->privilege = new PrivilegeDAO();
            $this->values = $this->privilege->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ?ActionDAO
    {
        return $this->action;
    }
    public function getPrivilege(): ?PrivilegeDAO
    {
        return $this->privilege;
    }


    public function setId(null|int|string $id): void
    {
        $this->id = (int) $id;
    }

    public function setAction(?ActionDAO $action): void
    {
        $this->action = $action;
    }
    public function setPrivilege(?PrivilegeDAO $privilege): void
    {
        $this->privilege = $privilege;
    }
}

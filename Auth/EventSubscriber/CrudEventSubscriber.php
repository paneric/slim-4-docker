<?php

declare(strict_types=1);

namespace Paneric\Auth\EventSubscriber;

use Paneric\ComponentModule\Infrastructure\Event\CreateFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\CreateSuccessEvent;
use Paneric\ComponentModule\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModule\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\UpdateSuccessEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeCreateEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\Logger\EventLogger;

class CrudEventSubscriber
{
    public function __construct(readonly private EventLogger $eventLogger)
    {
    }

    public function onBeforeCreate(BeforeCreateEvent $event): void
    {
        $this->eventLogger->info('JEST BEFORE CREATE');die;
    }
    public function onCreateFailure(CreateFailureEvent $event): void
    {
    }
    public function onCreateSuccess(CreateSuccessEvent $event): void
    {
    }

    public function onBeforeDelete(BeforeDeleteEvent $event): void
    {
    }
    public function onDeleteFailure(DeleteFailureEvent $event): void
    {
    }
    public function onDeleteSuccess(DeleteSuccessEvent $event): void
    {
    }

    public function onBeforeUpdate(BeforeUpdateEvent $event): void
    {
    }
    public function onUpdateFailure(UpdateFailureEvent $event): void
    {
    }
    public function onUpdateSuccess(UpdateSuccessEvent $event): void
    {
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Auth\Role\Model;

use Paneric\DataObject\DAO;

class RoleDAO extends DAO
{
    use RoleTrait;

    protected null|int|string $id;
    protected string $ref;
    protected string $pl;
    protected string $en;

    public function __construct()
    {
        $this->prefix = 'rle_';

        $this->setMaps();
    }


    public function getId(): null|int|string
    {
        return $this->id;
    }
    public function getRef(): string
    {
        return $this->ref;
    }
    public function getPl(): string
    {
        return $this->pl;
    }
    public function getEn(): string
    {
        return $this->en;
    }


    public function setId(int|string $id): void
    {
        $this->id = $id;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setPl(string $pl): void
    {
        $this->pl = $pl;
    }
    public function setEn(string $en): void
    {
        $this->en = $en;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Auth\Role\config;

use Paneric\Auth\Role\Model\RoleVLD;
use Paneric\Auth\Role\Model\RoleDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'role';
    protected string $prefix = 'rle_';

    protected string $uniqueKey = 'ref';//unique key

    protected bool $stringId = true;

    protected string $daoClass = RoleDAO::class;
    protected string $vldClass = RoleVLD::class;
}

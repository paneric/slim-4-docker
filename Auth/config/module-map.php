<?php

declare(strict_types=1);

return [
    'http://slim-4-docker.dev/api/' => [
        'action' => './../Auth/Action',
        'actions' => './../Auth/Action',
        'privilege' => './../Auth/Privilege',
        'privileges' => './../Auth/Privilege',
        'role' => './../Auth/Role',
        'roles' => './../Auth/Role',
        'credential' => './../Auth/Credential',
        'credentials' => './../Auth/Credential',

        'action-privilege' => './../Auth/ActionPrivilege',
        'action-privileges' => './../Auth/ActionPrivilege',
        'privilege-role' => './../Auth/PrivilegeRole',
        'privilege-roles' => './../Auth/PrivilegeRole',
        'role-credential' => './../Auth/RoleCredential',
        'role-credentials' => './../Auth/RoleCredential',
    ],
];

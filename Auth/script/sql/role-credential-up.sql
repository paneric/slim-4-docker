/* role can be related to multiple credentials, credential can have multiple roles --MANY TO MANY-- OK */
CREATE TABLE `role_credential` (
  `rlcr_id` int(11) UNSIGNED NOT NULL,
  `rlcr_role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rlcr_credential_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `role_credential`
  ADD PRIMARY KEY (`rlcr_id`),
  ADD UNIQUE KEY `rlcr_role_id_credential_id` (`rlcr_role_id`,`rlcr_credential_id`),
  ADD KEY `rlcr_role_id` (`rlcr_role_id`),
  ADD KEY `rlcr_credential_id` (`rlcr_credential_id`),
  MODIFY `rlcr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `role_credential_ibfk_1` FOREIGN KEY (`rlcr_role_id`) REFERENCES `role` (`rle_id`),
  ADD CONSTRAINT `role_credential_ibfk_2` FOREIGN KEY (`rlcr_credential_id`) REFERENCES `credential` (`crd_id`);

INSERT INTO `role_credential` (`rlcr_role_id`, `rlcr_credential_id`) VALUES
  ('61cdc99eb04530.63384654', '61cde0d7bb6207.36549796'),
  ('61cdc99eb04b57.70393517', '61cde0d7bb6340.38773023'),
  ('61cdc99eb04c95.20214177', '61cde0d7bb6482.22655261');

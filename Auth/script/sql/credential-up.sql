CREATE TABLE `credential` (
  `crd_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_gsm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_api_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_terms` tinyint(1) NOT NULL DEFAULT '0',
  `crd_is_active` tinyint(1) NOT NULL DEFAULT '0',
  `crd_activation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `crd_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `credential`
  ADD PRIMARY KEY (`crd_id`),
  ADD UNIQUE KEY `crd_email` (`crd_email`),
  ADD UNIQUE KEY `crd_gsm` (`crd_gsm`),
  ADD UNIQUE KEY `crd_password_hash` (`crd_password_hash`),
  ADD UNIQUE KEY `crd_api_token` (`crd_api_token`),
  ADD KEY `crd_terms` (`crd_terms`),
  ADD KEY `crd_is_active` (`crd_is_active`),
  ADD KEY `crd_activation_token` (`crd_activation_token`),
  ADD KEY `crd_reset_token` (`crd_reset_token`),
  ADD KEY `crd_created_at` (`crd_created_at`),
  ADD KEY `crd_updated_at` (`crd_updated_at`);

INSERT INTO `credential` (`crd_id`, `crd_email`, `crd_gsm`, crd_password_hash, crd_terms, crd_is_active) VALUES
  ('61cde0d7bb6207.36549796', 'admin@admin.com', '001 123 456','$2y$10$MD6JLPvJJx8U2sO./SzoUenb.LtYN91tL8fsXUBtOhZnBrUWRcBuu', 1, 1),
  ('61cde0d7bb6340.38773023', 'visitor@visitor.com', '002 123 456','$2y$10$pKS2DK5vk8400kOPdWuGS.7HDSt0DtydJFKanNAp.Nl5l7BvYjyrK', 1, 1),
  ('61cde0d7bb6482.22655261', 'user@user.com', '003 123 456','$2y$10$VXFE1yE27KZZhcSDWBmC2..TyOWxwn2ScxDFEFThQUY0QQrqU/Q2S', 1, 1),
  ('61cde0d7bb65c7.85233421', 'creator@creator.com', '004 123 456','$2y$10$km2jRIlgAeLk6uB56QOC8.Woj6/KT10cU3nPTxlm3MukR2j.iIHGW', 1, 1),
  ('61cde0d7bb66f4.76760883', 'reader@reader.com', '005 123 456','$2y$10$SU2LEueCagKTEpFZhj4dm.6VcZLwQ/q5oVOE.LkZI7LlRKykzO2aK', 1, 1),
  ('61cde0d7bb6839.99540437', 'updater@updater.com', '006 123 456','$2y$10$qpbsf23uttFghhjmJcZvfuCYTpUk/3/HjPMb1cCNlkoKDRNuFHxv2', 1, 1),
  ('61cde0d7bb6965.61339893', 'deleter@deleter.com', '007 123 456','$2y$10$Yp7ob24rzEb7y7Y1wkh4Me76PATM9bnYlWOwn9x3lWt4WUKbtJXwW', 1, 1);

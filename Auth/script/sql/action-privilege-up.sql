/* action can have multiple privileges, privilege can have multiple actions --MANY TO MANY-- OK */
CREATE TABLE `action_privilege` (
  `acpv_id` int(11) UNSIGNED NOT NULL,
  `acpv_action_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acpv_privilege_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `action_privilege`
  ADD PRIMARY KEY (`acpv_id`),
  ADD UNIQUE KEY `acpv_action_id_privilege_id` (`acpv_action_id`,`acpv_privilege_id`),
  ADD KEY `acpv_action_id` (`acpv_action_id`),
  ADD KEY `acpv_privilege_id` (`acpv_privilege_id`),
  MODIFY `acpv_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `action_privilege_ibfk_1` FOREIGN KEY (`acpv_action_id`) REFERENCES `action` (`acn_id`),
  ADD CONSTRAINT `action_privilege_ibfk_2` FOREIGN KEY (`acpv_privilege_id`) REFERENCES `privilege` (`prv_id`);

INSERT INTO `action_privilege` (`acpv_action_id`, `acpv_privilege_id`) VALUES
  ('61cde0d7bb39d4.83875354', '61cdc99eabea26.22466900'),
  ('61cde0d7bb5905.76298467', '61cdc99eabea26.22466900'),
  ('61cde0d7bb5b48.99308470', '61cdc99eb03bb0.63256543'),
  ('61cde0d7bb5cd5.31970602', '61cdc99eb03bb0.63256543'),
  ('61cde0d7bb5e32.47123488', '61cdc99eb03ed6.73816642'),
  ('61cde0d7bb5f80.25876386', '61cdc99eb03ed6.73816642'),
  ('61cde0d7bb6207.36549796', '61cdc99eb04175.71612486'),
  ('61cde0d7bb6340.38773023', '61cdc99eb04175.71612486'),
  ('61cde0d7bb6482.22655261', '61cdc99eb043f6.05121545'),
  ('61cde0d7bb65c7.85233421', '61cdc99eb043f6.05121545'),
  ('61cde0d7bb66f4.76760883', '61cdcfc5062d54.02699254'),
  ('61cde0d7bb6839.99540437', '61cdcfc5062d54.02699254');

/* privilege can be in multiples roles, role can have multiple privileges --MANY TO MANY-- OK */
CREATE TABLE `privilege_role` (
  `pvrl_id` int(11) UNSIGNED NOT NULL,
  `pvrl_privilege_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pvrl_role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `privilege_role`
  ADD PRIMARY KEY (`pvrl_id`),
  ADD UNIQUE KEY `pvrl_privilege_id_role_id` (`pvrl_privilege_id`,`pvrl_role_id`),
  ADD KEY `pvrl_privilege_id` (`pvrl_privilege_id`),
  ADD KEY `pvrl_role_id` (`pvrl_role_id`),
  MODIFY `pvrl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `privilege_role_ibfk_1` FOREIGN KEY (`pvrl_privilege_id`) REFERENCES `privilege` (`prv_id`),
  ADD CONSTRAINT `privilege_role_ibfk_2` FOREIGN KEY (`pvrl_role_id`) REFERENCES `role` (`rle_id`);

INSERT INTO `privilege_role` (`pvrl_privilege_id`, `pvrl_role_id`) VALUES
  ('61cdc99eabea26.22466900', '61cdc99eb04530.63384654'),
  ('61cdc99eb03bb0.63256543', '61cdc99eb04530.63384654'),
  ('61cdc99eb03ed6.73816642', '61cdc99eb04530.63384654'),
  ('61cdc99eb04175.71612486', '61cdc99eb04530.63384654'),
  ('61cdc99eb043f6.05121545', '61cdc99eb04530.63384654'),
  ('61cdcfc5062d54.02699254', '61cdc99eb04530.63384654');

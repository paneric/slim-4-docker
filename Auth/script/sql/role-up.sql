CREATE TABLE `role` (
  `rle_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rle_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `role`
  ADD PRIMARY KEY (`rle_id`),
  ADD UNIQUE KEY `rle_ref` (`rle_ref`);

INSERT INTO `role` (`rle_id`, `rle_ref`) VALUES
  ('61cdc99eb04530.63384654', 'Admin'),
  ('61cdc99eb04b57.70393517', 'Visitor'),
  ('61cdc99eb04c95.20214177', 'User'),
  ('61cdc99eb04671.40830496', 'Creator'),
  ('61cdc99eb047a0.24273448', 'Reader'),
  ('61cdc99eb048e4.31597857', 'Updater'),
  ('61cdc99eb04a18.64564872', 'Deleter');

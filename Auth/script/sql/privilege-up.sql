CREATE TABLE `privilege` (
  `prv_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prv_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `privilege`
  ADD PRIMARY KEY (`prv_id`),
  ADD UNIQUE KEY `prv_ref` (`prv_ref`);

INSERT INTO `privilege` (`prv_id`, `prv_ref`) VALUES
  ('61cdc99eabea26.22466900', 'action'),
  ('61cdc99eb03bb0.63256543', 'privilege'),
  ('61cdc99eb03ed6.73816642', 'role'),
  ('61cdc99eb04175.71612486', 'action-privilege'),
  ('61cdc99eb043f6.05121545', 'privilege-role'),
  ('61cdcfc5062d54.02699254', 'action-role');

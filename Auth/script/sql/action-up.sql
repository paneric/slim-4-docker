CREATE TABLE `action` (
  `acn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acn_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `action`
  ADD PRIMARY KEY (`acn_id`),
  ADD UNIQUE KEY `acn_ref` (`acn_ref`);

INSERT INTO `action` (`acn_id`, `acn_ref`) VALUES
  ('61cde0d7bb39d4.83875354', 'action.*'),
  ('61cde0d7bb5905.76298467', 'actions.*'),
  ('61cce41c2eb9b5.59298252', 'action.add'),
  ('61cce41c2ed9c5.02121739', 'actions.add'),
  ('61cce41c2edc17.85446129', 'action.remove'),
  ('61cce41c2eddb1.88335789', 'actions.remove'),
  ('61cce41c2edf13.66676340', 'actions.get'),
  ('61cce41c2ee062.13077463', 'actions.get-paginated'),
  ('61cce41c2ee1b4.44817277', 'action.get-by-id'),
  ('61cce41c2ee2f1.12777032', 'action.edit'),
  ('61cce41c2ee438.36852957', 'actions.edit'),
  ('61cde0d7bb5b48.99308470', 'privilege.*'),
  ('61cde0d7bb5cd5.31970602', 'privileges.*'),
  ('61cdc99eabea26.22466900', 'privilege.add'),
  ('61cdc99eb03908.22760762', 'privileges.add'),
  ('61cdc99eb03bb0.63256543', 'privilege.remove'),
  ('61cdc99eb03d65.90418426', 'privileges.remove'),
  ('61cdc99eb03ed6.73816642', 'privileges.get'),
  ('61cdc99eb04024.17170617', 'privileges.get-paginated'),
  ('61cdc99eb04175.71612486', 'privilege.get-by-id'),
  ('61cdc99eb042b0.74213766', 'privilege.edit'),
  ('61cdc99eb043f6.05121545', 'privileges.edit'),
  ('61cde0d7bb5e32.47123488', 'role.*'),
  ('61cde0d7bb5f80.25876386', 'roles.*'),
  ('61cdc99eb04530.63384654', 'role.add'),
  ('61cdc99eb04671.40830496', 'roles.add'),
  ('61cdc99eb047a0.24273448', 'role.remove'),
  ('61cdc99eb048e4.31597857', 'roles.remove'),
  ('61cdc99eb04a18.64564872', 'roles.get'),
  ('61cdc99eb04b57.70393517', 'roles.get-paginated'),
  ('61cdc99eb04c95.20214177', 'role.get-by-id'),
  ('61cdc99eb04dc5.03245484', 'role.edit'),
  ('61cdc99eb04f04.38648489', 'roles.edit'),
  ('61cde0d7bb6207.36549796', 'action-privilege.*'),
  ('61cde0d7bb6340.38773023', 'action-privileges.*'),
  ('61cde0d7bb6482.22655261', 'privilege-role.*'),
  ('61cde0d7bb65c7.85233421', 'privilege-roles.*'),
  ('61cde0d7bb66f4.76760883', 'action-role.*'),
  ('61cde0d7bb6839.99540437', 'action-roles.*');

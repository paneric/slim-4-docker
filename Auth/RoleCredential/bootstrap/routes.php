<?php

declare(strict_types=1);

$module = 'role-credential';

if (isset($routePrefix)) {
    require ROOT_FOLDER . 'vendor/paneric/component-module/src/bootstrap/' . $routePrefix . '-routes.php';
}

<?php

declare(strict_types=1);

namespace Paneric\Auth\RoleCredential\Model;

use Paneric\Auth\Role\Model\RoleDAO;
use Paneric\Auth\Credential\Model\CredentialDAO;
use Paneric\DataObject\ADAO;

class RoleCredentialADAO extends ADAO
{
    use RoleCredentialTrait;

    protected ?int $id;

    protected ?RoleDAO $role;
    protected ?CredentialDAO $credential;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'rlcr_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->role = new RoleDAO();
            $this->values = $this->role->hydrate($this->values);

            $this->credential = new CredentialDAO();
            $this->values = $this->credential->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?RoleDAO
    {
        return $this->role;
    }
    public function getCredential(): ?CredentialDAO
    {
        return $this->credential;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setRole(?RoleDAO $role): void
    {
        $this->role = $role;
    }
    public function setCredential(?CredentialDAO $credential): void
    {
        $this->credential = $credential;
    }
}

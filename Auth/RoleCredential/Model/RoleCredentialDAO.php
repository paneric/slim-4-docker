<?php

declare(strict_types=1);

namespace Paneric\Auth\RoleCredential\Model;

use Paneric\DataObject\DAO;

class RoleCredentialDAO extends DAO
{
    use RoleCredentialTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'rlcr_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

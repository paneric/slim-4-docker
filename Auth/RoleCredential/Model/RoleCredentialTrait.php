<?php

declare(strict_types=1);

namespace Paneric\Auth\RoleCredential\Model;

trait RoleCredentialTrait
{
    protected ?string $roleId;
    protected ?string $credentialId;


    public function getRoleId(): ?string
    {
        return $this->roleId;
    }
    public function getCredentialId(): ?string
    {
        return $this->credentialId;
    }


    public function setRoleId(?string $roleId): void
    {
        $this->roleId = $roleId;
    }
    public function setCredentialId(?string $credentialId): void
    {
        $this->credentialId = $credentialId;
    }
}

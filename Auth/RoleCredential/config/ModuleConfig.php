<?php

declare(strict_types=1);

namespace Paneric\Auth\RoleCredential\config;

use Paneric\Auth\RoleCredential\Model\RoleCredentialADAO;
use Paneric\Auth\RoleCredential\Model\RoleCredentialDAO;
use Paneric\Auth\RoleCredential\Model\RoleCredentialVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'role_credential';
    protected string $prefix = 'rlcr_';

    protected bool $stringId = false;

    protected array $uniqueKeys = ['role_id', 'credential_id'];//unique key

    protected string $adaoClass = RoleCredentialADAO::class;
    protected string $daoClass = RoleCredentialDAO::class;
    protected string $vldClass = RoleCredentialVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM role_credential AS mt
                        LEFT JOIN role AS stl ON mt.rlcr_role_id = stl.rle_id
                        LEFT JOIN credential AS str ON mt.rlcr_credential_id = str.crd_id
                ";
}

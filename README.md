# Slim 4 Course

## Files Structure

### Application

*public/index.php*
```php
<?php

declare(strict_types=1);

define('ROOT_FOLDER', dirname(__DIR__) . '/');

use Dotenv\Dotenv;
use Slim\App;

require __DIR__ . '/../vendor/autoload.php';

$dotEnv = Dotenv::createImmutable(ROOT_FOLDER);
$dotEnv->load();
define('ENV',$_ENV['ENV']);

$container = require ROOT_FOLDER . 'src/bootstrap/container.php';

$slim = $container->get(App::class);

require MODULE_FOLDER . 'bootstrap/middleware.php';
require MODULE_FOLDER . 'bootstrap/routes.php';

require ROOT_FOLDER . 'src/bootstrap/middleware.php';
require ROOT_FOLDER . 'src/bootstrap/routes.php';

$slim->run();
```

*src/bootstrap/container.php*
```php
<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Paneric\Local\Local;
use Paneric\ModuleResolver\DefinitionsCollector;
use Paneric\ModuleResolver\ModuleResolver;

try {
    $moduleResolverConfig = require ROOT_FOLDER . 'src/config/module-resolver-config.php';
    $moduleResolver = new ModuleResolver();
    $moduleFolder = $moduleResolver->setModuleFolderName(
        $_SERVER['REQUEST_URI'],
        $moduleResolverConfig
    );

    define('MODULE_FOLDER', $moduleFolder);
    
    $localValue = $moduleResolver->getLocal();
    $localConfig = require ROOT_FOLDER . 'src/config/local-config.php';
    $local = new Local();
    $localValue = $local->setValue(
        $localConfig[ENV],
        $moduleResolverConfig['local_map'],
        $_COOKIE, $localValue
    );

    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        ROOT_FOLDER . 'src/',
        MODULE_FOLDER,
        'config',
        $localConfig[ENV]['default_local'],
        ENV
    );

    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(false);
    $builder->addDefinitions($definitions);

    $container = $builder->build(); // Throws uncaught exception
    $container->set('local', $localValue);

    return $container;
} catch (Exception $e) {
    echo $e->getMessage();
}
```

*src/bootstrap/routes.php*
```php
<?php

declare(strict_types=1);
```

*src/config/settings/dev.php*
```php
<?php

declare(strict_types=1);

return [

    /*...*/

    'twig-settings' => [
        'templates_dirs' => [
            'app' => ROOT_FOLDER . 'src/templates/',
            'mod' => MODULE_FOLDER . 'templates/'
        ],
        'options' => [
            'debug' => true, /* "prod" false */
            'charset' => 'UTF-8',
            'strict_variables' => false,
            'autoescape' => 'html',
            'cache' => false, /* "prod" ROOT_FOLDER.'/var/cache/twig'*/
            'auto_reload' => null,
            'optimizations' => -1,
        ],
    ],
 
    /*...*/
];
```

*src/config/settings/validation.php*
> DELETED !!!

*src/templates/author*
> MOVED TO *src/Author/templates*

*src/templates/book*
> MOVED TO *src/Book/templates*


### Module Author

*src/Author/Controller/AuthorController.php*

```php
<?php

declare(strict_types=1);

namespace App\Author\Controller;

use App\Author\Action\AuthorAddAction;
use App\Author\Action\AuthorEditAction;
use App\Author\Action\AuthorGetAction;
use App\Author\Action\BookGetAllAction;
use App\Author\Action\AuthorRemoveAction;
use Paneric\CSRTriad\Controller\AppController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthorController extends AppController
{
    public function add(
        Request $request,
        Response $response,
        AuthorAddAction $authorAddAction
    ): Response {
        $result = $authorAddAction($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/author/get-all',
                200
            );
        }

        return $this->render(
            $response,
            '@mod/add.html.twig',
            $result
        );
    }

    public function edit(
        Request $request,
        Response $response,
        AuthorEditAction $authorEditAction,
        AuthorGetAction $authorGetAction,
        string $id
    ): Response {
        $result = $authorEditAction($request, (int) $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/author/get-all',
                200
            );
        }

        return $this->render(
            $response,
            '@mod/edit.html.twig',
            $authorGetAction((int) $id)
        );
    }

    public function get(
        Response $response,
        AuthorGetAction $authorGetAction,
        string $id
    ): Response {
        return $this->render(
            $response,
            '@mod/get.html.twig',
            $authorGetAction((int) $id)
        );
    }

    public function getAll(
        Response $response,
        BookGetAllAction $authorGetAllAction
    ): Response {
        return $this->render(
            $response,
            '@mod/get_all.html.twig',
            $authorGetAllAction()
        );
    }

    public function remove(
        Request $request,
        Response $response,
        AuthorRemoveAction $authorRemoveAction,
        string $id
    ): Response {
        $result = $authorRemoveAction($request, (int) $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/author/get-all',
                200
            );
        }

        return $this->render(
            $response,
            '@mod/remove.html.twig',
            ['id' => $id]
        );
    }
}
```


*src/Author/bootstrap/middleware.php*
```php
<?php

declare(strict_types=1);
```

*src/Author/bootstrap/routes.php*

```php
<?php

declare(strict_types=1);

use App\Author\Controller\CredentialController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;

if (isset($slim, $container)) {
    $slim->map(['GET', 'POST'], '/author/add', [CredentialController::class, 'add'])
        ->setName('author.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $slim->map(['GET', 'POST'], '/author/edit/{id}', [CredentialController::class, 'edit'])
        ->setName('author.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $slim->get('/api/author/get/{id}', [CredentialController::class, 'get'])
        ->setName('author.get');

    $slim->get('/api/author/get-all', [CredentialController::class, 'getAll'])
        ->setName('author.get-all');

    $slim->map(['GET', 'POST'],'/author/remove/{id}', [CredentialController::class, 'remove'])
        ->setName('author.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class));
}
```

*src/Author/config/dependencies/middleware.php*
```php
<?php

declare(strict_types=1);

return [];
```

*src/Author/config/dependencies/packages.php*
```php
<?php

declare(strict_types=1);

return [];
```

*src/Author/config/settings/dev.php*
```php
<?php

declare(strict_types=1);

return [];
```

*src/Author/config/settings/validation.php*

```php
<?php

declare(strict_types=1);

use App\Author\Model\AuthorDTO;

return [

    'validation' => [

        'author.add' => [
            'methods' => ['POST'],
            AuthorDTO::class => [
                'rules' => [
                    'name' => [
                        'required' => [],
                    ],
                    'surname' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        'author.edit' => [
            'methods' => ['POST'],
            AuthorDTO::class => [
                'rules' => [
                    'name' => [
                        'required' => [],
                    ],
                    'surname' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
```


### Module Book

*src/Book/Controller/BookController.php*

```php
<?php

declare(strict_types=1);

namespace App\Book\Controller;

use App\Author\Action\BookGetAllAction;
use App\Book\Action\BookAddAction;
use App\Book\Action\BookEditAction;
use App\Book\Action\BookGetAction;
use App\Book\Action\BookGetAllAction;
use App\Book\Action\BookRemoveAction;
use Paneric\CSRTriad\Controller\AppController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class BookController extends AppController
{
    public function add(
        Request $request,
        Response $response,
        BookAddAction $bookAddAction,
        BookGetAllAction $authorGetAllAction
    ): Response {
        $result = $bookAddAction($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/book/get-all',
                200
            );
        }

        return $this->render(
            $response,
            '@mod/add.html.twig',
            array_merge(
                $result,
                $authorGetAllAction()
            )
        );
    }

    public function edit(
        Request $request,
        Response $response,
        BookEditAction $bookEditAction,
        BookGetAction $bookGetAction,
        BookGetAllAction $authorGetAllAction,
        string $id
    ): Response {
        $result = $bookEditAction($request, (int) $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/book/get-all',
                200
            );
        }

        return $this->render(
            $response,
            '@mod/edit.html.twig',
            array_merge(
                $bookGetAction((int) $id),
                $authorGetAllAction()
            )
        );
    }

    public function get(
        Response $response,
        BookGetAction $bookGetAction,
        BookGetAllAction $authorGetAllAction,
        string $id
    ): Response {
        return $this->render(
            $response,
            '@mod/get.html.twig',
            array_merge(
                $bookGetAction((int) $id),
                $authorGetAllAction()
            )
        );
    }

    public function getAll(
        Response $response,
        BookGetAllAction $bookGetAllAction,
        BookGetAllAction $authorGetAllAction
    ): Response {
        return $this->render(
            $response,
            '@mod/get_all.html.twig',
            array_merge(
                $bookGetAllAction(),
                $authorGetAllAction()
            )
        );
    }

    public function remove(
        Request $request,
        Response $response,
        BookRemoveAction $bookRemoveAction,
        string $id
    ): Response {
        $result = $bookRemoveAction($request, (int) $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/book/get-all',
                200
            );
        }

        return $this->render(
            $response,
            '@mod/remove.html.twig',
            ['id' => $id]
        );
    }
}
```

*src/Book/bootstrap/middleware.php*
```php
<?php

declare(strict_types=1);
```

*src/Book/bootstrap/routes.php*
```php
<?php

declare(strict_types=1);

use App\Book\Controller\BookController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;

if (isset($slim, $container)) {
    $slim->map(['GET', 'POST'], '/book/add', [BookController::class, 'add'])
        ->setName('book.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $slim->map(['GET', 'POST'], '/book/edit/{id}', [BookController::class, 'edit'])
        ->setName('book.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $slim->get('/api/book/get/{id}', [BookController::class, 'get'])
        ->setName('book.get');

    $slim->get('/api/book/get-all', [BookController::class, 'getAll'])
        ->setName('book.get-all');

    $slim->map(['GET', 'POST'],'/book/remove/{id}', [BookController::class, 'remove'])
        ->setName('book.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class));
}
```

*src/Book/config/dependencies/middleware.php*
```php
<?php

declare(strict_types=1);

return [];
```

*src/Book/config/dependencies/packages.php*
```php
<?php

declare(strict_types=1);

return [];
```

*src/Book/config/settings/dev.php*
```php
<?php

declare(strict_types=1);

return [];
```

*src/Book/config/settings/validation.php*

```php
<?php

declare(strict_types=1);

use App\Book\Model\BookDTO;

return [

    'validation' => [

        'book.add' => [
            'methods' => ['POST'],
            BookDTO::class => [
                'rules' => [
                    'author_id' => [
                        'required' => [],
                    ],
                    'title' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        'book.edit' => [
            'methods' => ['POST'],
            BookDTO::class => [
                'rules' => [
                    'author_id' => [
                        'required' => [],
                    ],
                    'title' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
```

https://confluence.atlassian.com/bbkb/permission-denied-publickey-302811860.html

https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/
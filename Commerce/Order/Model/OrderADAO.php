<?php

declare(strict_types=1);

namespace Paneric\Commerce\Order\Model;

use Paneric\Commerce\ListStatusOrder\Model\ListStatusOrderDAO;
use Paneric\DataObject\ADAO;

class OrderADAO extends ADAO
{
    use OrderTrait;

    protected ?int $id;

    protected ?ListStatusOrderDAO $listStatusOrder;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'ord_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->listStatusOrder = new ListStatusOrderDAO();
            $this->values = $this->listStatusOrder->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListStatusOrder(): ?ListStatusOrderDAO
    {
        return $this->listStatusOrder;
    }


    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setListStatusOrder(?ListStatusOrderDAO $listStatusOrder): void
    {
        $this->listStatusOrder = $listStatusOrder;
    }
}

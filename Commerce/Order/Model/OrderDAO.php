<?php

declare(strict_types=1);

namespace Paneric\Commerce\Order\Model;

use Paneric\DataObject\DAO;

class OrderDAO extends DAO
{
    use OrderTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'ord_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

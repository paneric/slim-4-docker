<?php

declare(strict_types=1);

namespace Paneric\Commerce\Order\Model;

trait OrderTrait
{
    protected ?int $listStatusOrderId;

    protected string $ref;
    protected int $priceBrut;
    protected ?string $clientRemarks;
    protected ?string $remarks;


    public function getListStatusOrderId(): ?int
    {
        return $this->listStatusOrderId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function getPriceBrut(): int
    {
        return $this->priceBrut;
    }
    public function getClientRemarks(): ?string
    {
        return $this->clientRemarks;
    }
    public function getRemarks(): ?string
    {
        return $this->remarks;
    }


    public function setListStatusOrderId(int $listStatusOrderId): void
    {
        $this->listStatusOrderId = $listStatusOrderId;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setPriceBrut(int $priceBrut): void
    {
        $this->priceBrut = $priceBrut;
    }
    public function setClientRemarks(?string $clientRemarks): void
    {
        $this->clientRemarks = $clientRemarks;
    }
    public function setRemarks(?string $remarks): void
    {
        $this->remarks = $remarks;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Order\config;

use Paneric\Commerce\Order\Model\OrderADAO;
use Paneric\Commerce\Order\Model\OrderDAO;
use Paneric\Commerce\Order\Model\OrderVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'orders';
    protected string $prefix = 'ord_';

    protected string $left = 'ref';
    protected string $prefixLeft = '';

    protected string $right = 'ref';
    protected string $prefixRight = '';

    protected string $uniqueKey = 'ref';//unique key

    protected bool $stringId = false;

    protected string $adaoClass = OrderADAO::class;
    protected string $daoClass = OrderDAO::class;
    protected string $vldClass = OrderVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM orders AS ord
                        LEFT JOIN list_status_order AS lso ON ord.ord_list_status_order_id = lso.lso_id
                ";
}

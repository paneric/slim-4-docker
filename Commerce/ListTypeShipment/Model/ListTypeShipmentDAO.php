<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypeShipment\Model;

use Paneric\ComponentModule\Model\ListDAO;

class ListTypeShipmentDAO extends ListDAO
{
    public function __construct()
    {
        $this->prefix = 'lts_';

        $this->setMaps();
    }
}

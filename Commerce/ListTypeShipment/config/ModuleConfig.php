<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypeShipment\config;

use Paneric\Commerce\ListTypeShipment\Model\ListTypeShipmentVLD;
use Paneric\Commerce\ListTypeShipment\Model\ListTypeShipmentDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'list_type_shipment';
    protected string $prefix = 'lts_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ListTypeShipmentDAO::class;
    protected string $vldClass = ListTypeShipmentVLD::class;

    protected bool $stringId = false;
}

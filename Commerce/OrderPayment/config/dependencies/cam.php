<?php

declare(strict_types=1);

use Paneric\Commerce\OrderPayment\config\ModuleConfig;
use Paneric\ComponentModule\Infrastructure\ModulePersister;
use Paneric\ComponentModule\Infrastructure\ModuleQuery;
use Paneric\ComponentModule\Infrastructure\ModuleRepository;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleQueryInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\ComponentModule\Responder\ApiModuleResponder;
use Paneric\ComponentModule\Responder\ApiModuleResponderInterface;
use Paneric\ComponentModule\Responder\AppModuleResponder;
use Paneric\ComponentModule\Responder\AppModuleResponderInterface;
use Psr\Container\ContainerInterface;
use Paneric\DBAL\Manager;

return [
    ModuleRepositoryInterface::class => static function (ContainerInterface $c): ModuleRepository {
        return new ModuleRepository(
            $c->get(Manager::class),
            $c->get(ModuleConfigInterface::class),
        );
    },
    ModulePersisterInterface::class => static function (ContainerInterface $c): ModulePersister {
        return new ModulePersister(
            $c->get(Manager::class),
            $c->get(ModuleConfigInterface::class),
        );
    },
    ModuleQueryInterface::class => static function (ContainerInterface $c): ModuleQuery {
        return new ModuleQuery(
            $c->get(Manager::class),
            $c->get(ModuleConfigInterface::class),
        );
    },

    ModuleConfigInterface::class => static function (): ModuleConfig {
        return new ModuleConfig();
    },

    ApiModuleResponderInterface::class => static function (): ApiModuleResponder {
        return new ApiModuleResponder();
    },

    AppModuleResponderInterface::class => static function (): AppModuleResponder {
        return new AppModuleResponder();
    },
];

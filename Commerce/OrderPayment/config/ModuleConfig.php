<?php

declare(strict_types=1);

namespace Paneric\Commerce\OrderPayment\config;

use Paneric\Commerce\OrderPayment\Model\OrderPaymentADAO;
use Paneric\Commerce\OrderPayment\Model\OrderPaymentDAO;
use Paneric\Commerce\OrderPayment\Model\OrderPaymentVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'order_payment';
    protected string $prefix = 'orpy_';

    protected string $left = 'orders';
    protected string $prefixLeft = 'ord_';

    protected string $right = 'payment';
    protected string $prefixRight = 'pay_';

    protected bool $stringId = false;

    protected string $uniqueKey = '';//unique key

    protected string $adaoClass = OrderPaymentADAO::class;
    protected string $daoClass = OrderPaymentDAO::class;
    protected string $vldClass = OrderPaymentVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM order_payment AS orpy
                        LEFT JOIN orders AS ord ON orpy.orpy_order_id = ord.ord_id
                        LEFT JOIN payment AS pay ON orpy.orpy_payment_id = pay.pay_id
                ";
}

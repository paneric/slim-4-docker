<?php

declare(strict_types=1);

namespace Paneric\Commerce\OrderPayment\Model;

use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Interfaces\ValidatorInterface;
use Paneric\ComponentModule\Model\Interfaces\DataObjectInterface;

class OrderPaymentVLD implements ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function invoke(DataObjectInterface|array $dao): void
    {
        throw new ValidationException();
    }
}

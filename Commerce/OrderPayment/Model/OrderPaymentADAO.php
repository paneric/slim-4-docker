<?php

declare(strict_types=1);

namespace Paneric\Commerce\OrderPayment\Model;

use Paneric\Commerce\Order\Model\OrderDAO;
use Paneric\Commerce\Payment\Model\PaymentDAO;
use Paneric\DataObject\ADAO;

class OrderPaymentADAO extends ADAO
{
    use OrderPaymentTrait;

    protected ?int $id;

    protected ?OrderDAO $order;
    protected ?PaymentDAO $payment;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'orpy_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->order = new OrderDAO();
            $this->values = $this->order->hydrate($this->values);

            $this->payment = new PaymentDAO();
            $this->values = $this->payment->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?OrderDAO
    {
        return $this->order;
    }
    public function getPayment(): ?PaymentDAO
    {
        return $this->payment;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setOrder(?OrderDAO $order): void
    {
        $this->order = $order;
    }
    public function setPayment(?PaymentDAO $payment): void
    {
        $this->payment = $payment;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\OrderPayment\Model;

use Paneric\DataObject\DAO;

class OrderPaymentDAO extends DAO
{
    use OrderPaymentTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'orpy_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

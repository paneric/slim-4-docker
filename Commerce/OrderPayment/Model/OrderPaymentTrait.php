<?php

declare(strict_types=1);

namespace Paneric\Commerce\OrderPayment\Model;

trait OrderPaymentTrait
{
    protected ?int $orderId;
    protected ?int $paymentId;


    public function getOrderId(): ?int
    {
        return $this->orderId;
    }
    public function getPaymentId(): ?int
    {
        return $this->paymentId;
    }


    public function setOrderId(?int $orderId): void
    {
        $this->orderId = $orderId;
    }
    public function setPaymentId(?int $paymentId): void
    {
        $this->paymentId = $paymentId;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListStatusOrder\config;

use Paneric\Commerce\ListStatusOrder\Model\ListStatusOrderVLD;
use Paneric\Commerce\ListStatusOrder\Model\ListStatusOrderDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'list_status_order';
    protected string $prefix = 'lso_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ListStatusOrderDAO::class;
    protected string $vldClass = ListStatusOrderVLD::class;

    protected bool $stringId = false;
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListStatusOrder\Model;

use Paneric\ComponentModule\Model\ListDAO;

class ListStatusOrderDAO extends ListDAO
{
    public function __construct()
    {
        $this->prefix = 'lso_';

        $this->setMaps();
    }
}

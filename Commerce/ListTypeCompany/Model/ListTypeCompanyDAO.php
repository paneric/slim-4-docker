<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypeCompany\Model;

use Paneric\ComponentModule\Model\ListDAO;

class ListTypeCompanyDAO extends ListDAO
{
    public function __construct()
    {
        $this->prefix = 'ltc_';

        $this->setMaps();
    }
}

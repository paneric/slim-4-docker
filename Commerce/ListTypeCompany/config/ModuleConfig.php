<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypeCompany\config;

use Paneric\Commerce\ListTypeCompany\Model\ListTypeCompanyVLD;
use Paneric\Commerce\ListTypeCompany\Model\ListTypeCompanyDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'list_type_company';
    protected string $prefix = 'ltc_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ListTypeCompanyDAO::class;
    protected string $vldClass = ListTypeCompanyVLD::class;

    protected bool $stringId = false;
}

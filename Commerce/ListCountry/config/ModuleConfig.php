<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListCountry\config;

use Paneric\Commerce\ListCountry\Model\ListCountryVLD;
use Paneric\Commerce\ListCountry\Model\ListCountryDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'list_country';
    protected string $prefix = 'lc_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ListCountryDAO::class;
    protected string $vldClass = ListCountryVLD::class;

    protected bool $stringId = false;
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListCountry\Model;

use Paneric\ComponentModule\Model\ListDAO;

class ListCountryDAO extends ListDAO
{
    public function __construct()
    {
        $this->prefix = 'lc_';

        $this->setMaps();
    }
}

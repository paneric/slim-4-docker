<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypeTransaction\Model;

use Paneric\ComponentModule\Model\ListDAO;

class ListTypeTransactionDAO extends ListDAO
{
    public function __construct()
    {
        $this->prefix = 'ltt_';

        $this->setMaps();
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypeTransaction\config;

use Paneric\Commerce\ListTypeTransaction\Model\ListTypeTransactionVLD;
use Paneric\Commerce\ListTypeTransaction\Model\ListTypeTransactionDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'list_type_transaction';
    protected string $prefix = 'ltt_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ListTypeTransactionDAO::class;
    protected string $vldClass = ListTypeTransactionVLD::class;

    protected bool $stringId = false;
}

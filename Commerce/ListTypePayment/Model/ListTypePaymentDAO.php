<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypePayment\Model;

use Paneric\ComponentModule\Model\ListDAO;

class ListTypePaymentDAO extends ListDAO
{
    public function __construct()
    {
        $this->prefix = 'ltp_';

        $this->setMaps();
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\ListTypePayment\config;

use Paneric\Commerce\ListTypePayment\Model\ListTypePaymentVLD;
use Paneric\Commerce\ListTypePayment\Model\ListTypePaymentDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'list_type_payment';
    protected string $prefix = 'ltp_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ListTypePaymentDAO::class;
    protected string $vldClass = ListTypePaymentVLD::class;

    protected bool $stringId = false;
}

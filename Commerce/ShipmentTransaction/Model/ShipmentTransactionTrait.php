<?php

declare(strict_types=1);

namespace Paneric\Commerce\ShipmentTransaction\Model;

trait ShipmentTransactionTrait
{
    protected ?int $shipmentId;
    protected ?int $transactionId;


    public function getShipmentId(): ?int
    {
        return $this->shipmentId;
    }
    public function getTransactionId(): ?int
    {
        return $this->transactionId;
    }


    public function setShipmentId(?int $shipmentId): void
    {
        $this->shipmentId = $shipmentId;
    }
    public function setTransactionId(?int $transactionId): void
    {
        $this->transactionId = $transactionId;
    }
}

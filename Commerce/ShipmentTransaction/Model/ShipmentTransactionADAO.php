<?php

declare(strict_types=1);

namespace Paneric\Commerce\ShipmentTransaction\Model;

use Paneric\Commerce\Shipment\Model\ShipmentDAO;
use Paneric\Commerce\Transaction\Model\TransactionDAO;
use Paneric\DataObject\ADAO;

class ShipmentTransactionADAO extends ADAO
{
    use ShipmentTransactionTrait;

    protected ?int $id;

    protected ?ShipmentDAO $shipment;
    protected ?TransactionDAO $transaction;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'shtr_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->shipment = new ShipmentDAO();
            $this->values = $this->shipment->hydrate($this->values);

            $this->transaction = new TransactionDAO();
            $this->values = $this->transaction->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShipment(): ?ShipmentDAO
    {
        return $this->shipment;
    }
    public function getTransaction(): ?TransactionDAO
    {
        return $this->transaction;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setShipment(?ShipmentDAO $shipment): void
    {
        $this->shipment = $shipment;
    }
    public function setTransaction(?TransactionDAO $transaction): void
    {
        $this->transaction = $transaction;
    }
}

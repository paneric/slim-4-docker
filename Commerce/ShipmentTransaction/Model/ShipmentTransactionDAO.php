<?php

declare(strict_types=1);

namespace Paneric\Commerce\ShipmentTransaction\Model;

use Paneric\DataObject\DAO;

class ShipmentTransactionDAO extends DAO
{
    use ShipmentTransactionTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'shtr_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

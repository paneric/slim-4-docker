<?php

declare(strict_types=1);

namespace Paneric\Commerce\ShipmentTransaction\config;

use Paneric\Commerce\ShipmentTransaction\Model\ShipmentTransactionADAO;
use Paneric\Commerce\ShipmentTransaction\Model\ShipmentTransactionDAO;
use Paneric\Commerce\ShipmentTransaction\Model\ShipmentTransactionVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'shipment_transaction';
    protected string $prefix = 'shtr_';

    protected string $left = 'shipment';
    protected string $prefixLeft = 'shp_';

    protected string $right = 'transaction';
    protected string $prefixRight = 'trn_';

    protected bool $stringId = false;

    protected string $uniqueKey = 'shipment_id';//unique key

    protected string $adaoClass = ShipmentTransactionADAO::class;
    protected string $daoClass = ShipmentTransactionDAO::class;
    protected string $vldClass = ShipmentTransactionVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM shipment_transaction AS shtr
                        LEFT JOIN shipment AS shp ON shtr.shtr_shipment_id = shp.shp_id
                        LEFT JOIN transaction AS trn ON shtr.shtr_transaction_id = trn.trn_id
                ";
}

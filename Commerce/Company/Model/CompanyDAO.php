<?php

declare(strict_types=1);

namespace Paneric\Commerce\Company\Model;

use Paneric\DataObject\DAO;

class CompanyDAO extends DAO
{
    use CompanyTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'cmp_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Company\Model;

use Paneric\Commerce\ListTypeCompany\Model\ListTypeCompanyDAO;
use Paneric\DataObject\ADAO;

class CompanyADAO extends ADAO
{
    use CompanyTrait;

    protected ?int $id;

    protected ?ListTypeCompanyDAO $listTypeCompany;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'cmp_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->listTypeCompany = new ListTypeCompanyDAO();
            $this->values = $this->listTypeCompany->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListTypeCompany(): ?ListTypeCompanyDAO
    {
        return $this->listTypeCompany;
    }


    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setListTypeCompany(?ListTypeCompanyDAO $listTypeCompany): void
    {
        $this->listTypeCompany = $listTypeCompany;
    }
}

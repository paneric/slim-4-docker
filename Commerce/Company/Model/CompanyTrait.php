<?php

declare(strict_types=1);

namespace Paneric\Commerce\Company\Model;

trait CompanyTrait
{
    protected ?int $listTypeCompanyId;

    protected string $ref;
    protected string $name;

    public function getListTypeCompanyId(): ?int
    {
        return $this->listTypeCompanyId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function getName(): string
    {
        return $this->name;
    }


    public function setListTypeCompanyId(int $listTypeCompanyId): void
    {
        $this->listTypeCompanyId = $listTypeCompanyId;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}

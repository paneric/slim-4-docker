<?php

declare(strict_types=1);

namespace Paneric\Commerce\Company\config;

use JetBrains\PhpStorm\ArrayShape;
use Paneric\Commerce\Company\Model\CompanyADAO;
use Paneric\Commerce\Company\Model\CompanyDAO;
use Paneric\Commerce\Company\Model\CompanyVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use PDO;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'company';
    protected string $prefix = 'cmp_';

    protected string $left = 'ref';
    protected string $prefixLeft = '';

    protected string $right = 'name';
    protected string $prefixRight = '';

    protected string $uniqueKey = 'name';//unique key

    protected bool $stringId = false;

    protected string $adaoClass = CompanyADAO::class;
    protected string $daoClass = CompanyDAO::class;
    protected string $vldClass = CompanyVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM company AS mt
                        LEFT JOIN list_type_company AS stl ON mt.cmp_list_type_company_id = stl.ltc_id
                ";

    #[ArrayShape([
        'table' => "string",
        'dto_class' => "string",
        'fetch_mode' => "int",
        'create_unique_where' => "string",
        'update_unique_where' => "string"
    ])]
    public function persister(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->daoClass,
            'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            'create_unique_where' => sprintf(
                ' WHERE %s_id=:%s_id OR %s_id=:%s_id',
                $this->prefix . $this->left,
                $this->prefix . $this->left,
                $this->prefix . $this->right,
                $this->prefix . $this->right,
            ),
            'update_unique_where' => sprintf(
                ' (WHERE %s_id=:%s_id OR %s_id=:%s_id) AND %sid NOT IN (:%sid)',
                $this->prefix . $this->left,
                $this->prefix . $this->left,
                $this->prefix . $this->right,
                $this->prefix . $this->right,
                $this->prefix,
                $this->prefix
            ),
        ];
    }
}

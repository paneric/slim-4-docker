<?php

declare(strict_types=1);

namespace Paneric\Commerce\Transaction\Model;

use Paneric\Commerce\ListTypeTransaction\Model\ListTypeTransactionDAO;
use Paneric\DataObject\ADAO;
use Paneric\PIM\Product\Model\ProductDAO;

class TransactionADAO extends ADAO
{
    use TransactionTrait;

    protected ?int $id;

    protected ?ProductDAO $product;
    protected ?ListTypeTransactionDAO $listTypeTransaction;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'trn_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->listTypeTransaction = new ListTypeTransactionDAO();
            $this->values = $this->listTypeTransaction->hydrate($this->values);

            $this->product = new ProductDAO();
            $this->values = $this->product->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListTypeTransaction(): ?ListTypeTransactionDAO
    {
        return $this->listTypeTransaction;
    }
    public function getProduct(): ?ProductDAO
    {
        return $this->product;
    }


    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setListTypeTransaction(?ListTypeTransactionDAO $listTypeTransaction): void
    {
        $this->listTypeTransaction = $listTypeTransaction;
    }
    public function setProduct(?ProductDAO $product): void
    {
        $this->product = $product;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Transaction\Model;

use Paneric\DataObject\DAO;

class TransactionDAO extends DAO
{
    use TransactionTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'trn_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Transaction\Model;

trait TransactionTrait
{
    protected ?int $listTypeTransactionId;
    protected ?int $productId;

    protected string $ref;
    protected int $quantity;
    protected int $priceNet;
    protected int $priceBrut;


    public function getListTypeTransactionId(): ?int
    {
        return $this->listTypeTransactionId;
    }
    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function getQuantity(): int
    {
        return $this->quantity;
    }
    public function getPriceNet(): int
    {
        return $this->priceNet;
    }
    public function getPriceBrut(): int
    {
        return $this->priceBrut;
    }


    public function setListTypeTransactionId(int $listTypeTransactionId): void
    {
        $this->listTypeTransactionId = $listTypeTransactionId;
    }
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }
    public function setPriceNet(int $priceNet): void
    {
        $this->priceNet = $priceNet;
    }
    public function setPriceBrut(int $priceBrut): void
    {
        $this->priceBrut = $priceBrut;
    }
}

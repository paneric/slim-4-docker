<?php

declare(strict_types=1);

namespace Paneric\Commerce\Transaction\config;

use Paneric\Commerce\Transaction\Model\TransactionADAO;
use Paneric\Commerce\Transaction\Model\TransactionDAO;
use Paneric\Commerce\Transaction\Model\TransactionVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'transaction';
    protected string $prefix = 'trn_';

    protected string $left = 'ref';
    protected string $prefixLeft = '';

    protected string $right = 'ref';
    protected string $prefixRight = '';

    protected string $uniqueKey = 'ref';//unique key

    protected bool $stringId = false;

    protected string $adaoClass = TransactionADAO::class;
    protected string $daoClass = TransactionDAO::class;
    protected string $vldClass = TransactionVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM transaction AS mt
                        LEFT JOIN list_type_transaction AS stl ON mt.trn_list_type_transaction_id = stl.ltt_id
                        LEFT JOIN product AS str ON mt.trn_product_id = str.prd_id
                ";
}

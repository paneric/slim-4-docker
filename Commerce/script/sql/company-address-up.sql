# company can have multiple addresses, address can have multiple companies - --MANY TO MANY-- OK
CREATE TABLE `company_address` (
    `cpad_id` int(11) UNSIGNED NOT NULL,
    `cpad_company_id` int(11) UNSIGNED NOT NULL,
    `cpad_address_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `company_address`
    ADD PRIMARY KEY (`cpad_id`),
    ADD UNIQUE KEY `cpad_company_id_address_id` (`cpad_company_id`,`cpad_address_id`),
    ADD KEY `cpad_company_id` (`cpad_company_id`),
    ADD KEY `cpad_address_id` (`cpad_address_id`),
    MODIFY `cpad_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
    ADD CONSTRAINT `company_address_ibfk_1` FOREIGN KEY (`cpad_company_id`) REFERENCES `company` (`cmp_id`),
    ADD CONSTRAINT `company_address_ibfk_2` FOREIGN KEY (`cpad_address_id`) REFERENCES `address` (`adr_id`);

INSERT INTO `company_address` (`cpad_company_id`, `cpad_address_id`) VALUES
    (1, 1),
    (2, 2),
    (3, 3);

CREATE TABLE `list_type_company` (
  `ltc_id` int(11) UNSIGNED NOT NULL,
  `ltc_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltc_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltc_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltc_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ltc_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `list_type_company`
  ADD PRIMARY KEY (`ltc_id`),
  ADD UNIQUE KEY `ltc_ref` (`ltc_ref`),
  ADD KEY `ltc_created_at` (`ltc_created_at`),
  ADD KEY `ltc_updated_at` (`ltc_updated_at`),
  MODIFY `ltc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `list_type_company` (`ltc_id`, `ltc_ref`, `ltc_pl`, `ltc_en`, `ltc_created_at`, `ltc_updated_at`) VALUES
  (1, 'no', '-', '-', '2020-11-25 15:01:42', '2021-05-15 13:20:55'),
  (2, 'spc', 'sp. c.', 'sp. c.', '2020-11-25 15:01:42', '2021-02-03 00:51:54'),
  (3, 'spj', 'sp. j.', 'sp. j.', '2020-11-25 15:02:28', '2020-12-03 11:05:45'),
  (4, 'spp', 'sp. p.', 'sp. p.', '2020-11-25 15:02:28', '2020-12-03 11:05:49'),
  (5, 'spk', 'sp. k.', 'sp. k.', '2020-11-25 15:03:23', '2020-12-03 11:05:53'),
  (6, 'ska', 'S. K. A.', 'S. K. A.', '2020-11-25 15:03:23', '2020-12-03 11:05:58'),
  (7, 'spzoo', 'sp. z o.o.', 'sp. z o.o.', '2020-11-25 15:05:13', '2020-12-03 11:06:02'),
  (8, 'sa', 'S. A.', 'S. A.', '2020-11-25 15:05:13', '2020-12-03 11:06:05');

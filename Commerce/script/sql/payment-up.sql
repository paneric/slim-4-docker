CREATE TABLE `payment` (
  `pay_id` int(11) UNSIGNED NOT NULL,
  `pay_ref` varchar(255) COLLATE utf8_unicode_ci,
  `pay_list_type_payment_id` int(11) UNSIGNED NOT NULL,
  `pay_amount` int(11) UNSIGNED NOT NULL,
  `pay_communication` varchar(255) COLLATE utf8_unicode_ci,
  `pay_remarks` varchar(255) COLLATE utf8_unicode_ci,
  `pay_date` datetime NOT NULL,
  `pay_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pay_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `payment`
  ADD PRIMARY KEY (`pay_id`),
  ADD UNIQUE KEY `pay_ref` (`pay_ref`),
  ADD KEY `pay_list_type_payment_id` (`pay_list_type_payment_id`) USING BTREE,
  ADD KEY `pay_amount` (`pay_amount`),
  ADD KEY `pay_date` (`pay_date`),
  ADD KEY `pay_created_at` (`pay_created_at`),
  ADD KEY `pay_updated_at` (`pay_updated_at`),
  MODIFY `pay_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `payer_ibfk_1` FOREIGN KEY (`pay_list_type_payment_id`) REFERENCES `list_type_payment` (`ltp_id`);

INSERT INTO `payment` (`pay_ref`, `pay_list_type_payment_id`, `pay_amount`, `pay_date`) VALUES
  ('pay1', '1', 1000, '2022-02-23 23:14:33'),
  ('pay2', '2', 2000, '2022-02-23 23:14:33'),
  ('pay3', '3', 3000, '2022-02-23 23:14:33');

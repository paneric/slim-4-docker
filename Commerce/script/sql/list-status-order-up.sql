CREATE TABLE `list_status_order` (
  `lso_id` int(11) UNSIGNED NOT NULL,
  `lso_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lso_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lso_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lso_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lso_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `list_status_order`
  ADD PRIMARY KEY (`lso_id`),
  ADD UNIQUE KEY `lso_ref` (`lso_ref`),
  ADD KEY `lso_created_at` (`lso_created_at`),
  ADD KEY `lso_updated_at` (`lso_updated_at`),
  MODIFY `lso_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `list_status_order` (`lso_ref`, `lso_pl`, `lso_en`) VALUES
  ('cart', 'koszyk', 'cart'),
  ('realization', 'realizacja', 'realization'),
  ('shipment', 'wysyłka', 'shipment'),
  ('completed', 'zrealizowane', 'completed');

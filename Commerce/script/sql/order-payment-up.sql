# order can have multiple payments, payment can be for multiple orders - --MANY TO MANY-- OK
CREATE TABLE `order_payment` (
  `orpy_id` int(11) UNSIGNED NOT NULL,
  `orpy_order_id` int(11) UNSIGNED NOT NULL,
  `orpy_payment_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `order_payment`
  ADD PRIMARY KEY (`orpy_id`),
  ADD UNIQUE KEY `orpy_order_id_payment_id` (`orpy_order_id`,`orpy_payment_id`),
  ADD KEY `orpy_order_id` (`orpy_order_id`),
  ADD KEY `orpy_payment_id` (`orpy_payment_id`),
  MODIFY `orpy_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `order_payment_ibfk_1` FOREIGN KEY (`orpy_order_id`) REFERENCES `order` (`ord_id`),
  ADD CONSTRAINT `order_payment_ibfk_2` FOREIGN KEY (`orpy_payment_id`) REFERENCES `payment` (`pay_id`);

INSERT INTO `order_payment` (`orpy_order_id`, `orpy_payment_id`) VALUES
  (1, 1),
  (2, 2),
  (3, 3);

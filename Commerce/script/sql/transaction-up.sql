CREATE TABLE `transaction` (
  `trn_id` int(11) UNSIGNED NOT NULL,
  `trn_ref` varchar(255) COLLATE utf8_unicode_ci,
  `trn_list_type_transaction_id` int(11) UNSIGNED NOT NULL,
  `trn_quantity` int(11) NOT NULL,
  `trn_price_net` int(11) NOT NULL,
  `trn_price_brut` int(11) NOT NULL,
  `trn_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trn_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `transaction`
  ADD PRIMARY KEY (`trn_id`),
  ADD UNIQUE KEY `trn_ref` (`trn_ref`),
  ADD KEY `trn_list_type_transaction_id` (`trn_list_type_transaction_id`) USING BTREE,
  ADD KEY `trn_quantity` (`trn_quantity`),
  ADD KEY `trn_created_at` (`trn_created_at`),
  ADD KEY `trn_updated_at` (`trn_updated_at`),
  MODIFY `trn_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`trn_list_type_transaction_id`) REFERENCES `list_type_transaction` (`ltt_id`);

INSERT INTO `transaction` (`trn_id`, `trn_ref`, `trn_list_type_transaction_id`, `trn_quantity`, `trn_price_net`, `trn_price_brut`) VALUES
  (1, 'trans1', '1', '100', '100', '121'),
  (2, 'trans2', '1', '200', '100', '242'),
  (3, 'trans3', '1', '300', '100', '363'),
  (4, 'trans4', '1', '400', '100', '484'),
  (5, 'trans5', '1', '500', '100', '505');

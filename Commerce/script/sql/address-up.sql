CREATE TABLE `address` (
  `adr_id` int(11) UNSIGNED NOT NULL,
  `adr_list_country_id` int(11) UNSIGNED NOT NULL,
  `adr_street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adr_po_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adr_po_box` varchar(255) COLLATE utf8_unicode_ci,
  `adr_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adr_po_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adr_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adr_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `address`
  ADD PRIMARY KEY (`adr_id`),
  ADD KEY `adr_country_id` (`adr_list_country_id`) USING BTREE,
  ADD KEY `adr_street` (`adr_street`),
  ADD KEY `adr_po_number` (`adr_po_number`),
  ADD KEY `adr_po_box` (`adr_po_box`),
  ADD KEY `adr_city` (`adr_city`),
  ADD KEY `adr_po_code` (`adr_po_code`),
  ADD KEY `adr_created_at` (`adr_created_at`),
  ADD KEY `adr_updated_at` (`adr_updated_at`),
  MODIFY `adr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `address_ibfk_2` FOREIGN KEY (`adr_list_country_id`) REFERENCES `list_country` (`lc_id`);

INSERT INTO `address` (`adr_id`, `adr_list_country_id`, `adr_street`, `adr_po_number`, `adr_po_box`, `adr_city`, `adr_po_code`) VALUES
  (1, 1, 'Av. Rogier', 1457, NULL, 'Brussels', '1000'),
  (2, 13, 'Downing Street', 98, NULL, 'Dublin', '20586'),
  (3, 20, 'ul. Woronicza', 45, NULL, 'Warszawa', '00-950');

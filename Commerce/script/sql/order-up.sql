CREATE TABLE `order` (
  `ord_id` int(11) UNSIGNED NOT NULL,
  `ord_ref` varchar(255) COLLATE utf8_unicode_ci,
  `ord_list_status_order_id` int(11) UNSIGNED NOT NULL,
  `ord_price_brut` int(11) UNSIGNED NOT NULL,
  `ord_client_remarks` varchar(255) COLLATE utf8_unicode_ci,
  `ord_remarks` varchar(255) COLLATE utf8_unicode_ci,
  `ord_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ord_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `order`
  ADD PRIMARY KEY (`ord_id`),
  ADD UNIQUE KEY `ord_ref` (`ord_ref`),
  ADD KEY `ord_list_status_order_id` (`ord_list_status_order_id`) USING BTREE,
  ADD KEY `ord_price_brut` (`ord_price_brut`),
  ADD KEY `ord_created_at` (`ord_created_at`),
  ADD KEY `ord_updated_at` (`ord_updated_at`);

ALTER TABLE `order`
  MODIFY `ord_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`ord_list_status_order_id`) REFERENCES `list_status_order` (`lso_id`);

INSERT INTO `order` (`ord_id`,`ord_ref`, `ord_list_status_order_id`, `ord_price_brut`) VALUES
  (1, 'ord1', '1', 1000),
  (2, 'ord2', '2', 2000),
  (3, 'ord3', '4', 3000);

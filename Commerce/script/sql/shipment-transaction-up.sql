# shipment can have multiple transactions, transaction can have one shipment --MANY TO ONE-- OK
CREATE TABLE `shipment_transaction` (
  `shtr_id` int(11) UNSIGNED NOT NULL,
  `shtr_shipment_id` int(11) UNSIGNED NOT NULL,
  `shtr_transaction_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `shipment_transaction`
  ADD PRIMARY KEY (`shtr_id`),
  ADD KEY `shtr_shipment_id` (`shtr_shipment_id`),
  ADD UNIQUE KEY `shtr_transaction_id` (`shtr_transaction_id`),
  MODIFY `shtr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `shipment_transaction_ibfk_1` FOREIGN KEY (`shtr_shipment_id`) REFERENCES `shipment` (`shp_id`),
  ADD CONSTRAINT `shipment_transaction_ibfk_2` FOREIGN KEY (`shtr_transaction_id`) REFERENCES `transaction` (`trn_id`);

INSERT INTO `shipment_transaction` (`shtr_shipment_id`, `shtr_transaction_id`) VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (4, 4),
  (5, 5);

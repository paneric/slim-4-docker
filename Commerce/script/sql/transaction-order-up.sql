# transaction can have one order, order can have multiple transactions - --ONE TO MANY-- OK
CREATE TABLE `transaction_order` (
    `tror_id` int(11) UNSIGNED NOT NULL,
    `tror_transaction_id` int(11) UNSIGNED NOT NULL,
    `tror_order_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `transaction_order`
    ADD PRIMARY KEY (`tror_id`),
    ADD UNIQUE KEY `tror_transaction_id` (`tror_transaction_id`),
    ADD KEY `tror_order_id` (`tror_order_id`),
    MODIFY `tror_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
    ADD CONSTRAINT `transaction_order_ibfk_1` FOREIGN KEY (`tror_transaction_id`) REFERENCES `transaction` (`trn_id`),
    ADD CONSTRAINT `transaction_order_ibfk_2` FOREIGN KEY (`tror_order_id`) REFERENCES `order` (`ord_id`);

INSERT INTO `transaction_order` (`tror_transaction_id`, `tror_order_id`) VALUES
    (1, 1),
    (2, 2),
    (3, 2),
    (4, 3),
    (5, 3);

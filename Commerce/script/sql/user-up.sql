CREATE TABLE `user` (
  `usr_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usr_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usr_first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usr_last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usr_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usr_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `user`
  ADD PRIMARY KEY (`usr_id`),
  ADD UNIQUE KEY `usr_ref_unique` (`usr_ref`),
  ADD KEY `usr_first_name` (`usr_first_name`),
  ADD KEY `usr_last_name` (`usr_last_name`),
  ADD KEY `usr_created_at` (`usr_created_at`),
  ADD KEY `usr_updated_at` (`usr_updated_at`);

INSERT INTO `user` (`usr_id`, `usr_ref`, `usr_first_name`, `usr_last_name`) VALUES
  ('61dad6c50a2057.22589577', 'F1L1', 'Firstname1', 'Lastname1'),
  ('61dad6c50a2086.05089964', 'F2L2', 'Firstname2', 'Lastname2'),
  ('61dad6c50a2091.41486853', 'F3L3', 'Firstname3', 'Lastname3');

CREATE TABLE `list_type_shipment` (
  `lts_id` int(11) UNSIGNED NOT NULL,
  `lts_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lts_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lts_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lts_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lts_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `list_type_shipment`
  ADD PRIMARY KEY (`lts_id`),
  ADD UNIQUE KEY `lts_ref` (`lts_ref`),
  ADD KEY `lts_created_at` (`lts_created_at`),
  ADD KEY `lts_updated_at` (`lts_updated_at`),
  MODIFY `lts_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `list_type_shipment` (`lts_ref`, `lts_pl`, `lts_en`) VALUES
  ('client_collection', 'Odbiór przez klienta', 'Collection by client'),
  ('self_delivery', 'Dostawa własna', 'Self delivery'),
  ('dpd', 'DPD', 'DPD'),
  ('ups', 'UPS', 'UPS');

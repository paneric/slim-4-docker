# address can have multiple shipments, shipment can have one address --MANY TO ONE-- OK
CREATE TABLE `address_shipment` (
  `adsh_id` int(11) UNSIGNED NOT NULL,
  `adsh_address_id` int(11) UNSIGNED NOT NULL,
  `adsh_shipment_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `address_shipment`
  ADD PRIMARY KEY (`adsh_id`),
  ADD KEY `adsh_address_id` (`adsh_address_id`),
  ADD UNIQUE KEY `adsh_shipment_id` (`adsh_shipment_id`),
  MODIFY `adsh_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `address_shipment_ibfk_1` FOREIGN KEY (`adsh_address_id`) REFERENCES `address` (`adr_id`),
  ADD CONSTRAINT `address_shipment_ibfk_2` FOREIGN KEY (`adsh_shipment_id`) REFERENCES `shipment` (`shp_id`);

INSERT INTO `address_shipment` (`adsh_address_id`, `adsh_shipment_id`) VALUES
  (1, 1),
  (2, 2),
  (2, 3),
  (3, 4),
  (3, 5);

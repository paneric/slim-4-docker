CREATE TABLE `list_type_payment` (
  `ltp_id` int(11) UNSIGNED NOT NULL,
  `ltp_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltp_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltp_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltp_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ltp_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `list_type_payment`
  ADD PRIMARY KEY (`ltp_id`),
  ADD UNIQUE KEY `ltp_ref` (`ltp_ref`),
  ADD KEY `ltp_created_at` (`ltp_created_at`),
  ADD KEY `ltp_updated_at` (`ltp_updated_at`),
  MODIFY `ltp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `list_type_payment` (`ltp_ref`, `ltp_pl`, `ltp_en`) VALUES
  ('cash', 'gotówka', 'cash'),
  ('debit_card', 'karta debetowa', 'debit card'),
  ('credit_card', 'karta kredytowa', 'credit card');

CREATE TABLE `list_type_transaction` (
  `ltt_id` int(11) UNSIGNED NOT NULL,
  `ltt_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltt_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltt_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ltt_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ltt_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `list_type_transaction`
  ADD PRIMARY KEY (`ltt_id`),
  ADD UNIQUE KEY `ltt_ref` (`ltt_ref`),
  ADD KEY `ltt_created_at` (`ltt_created_at`),
  ADD KEY `ltt_updated_at` (`ltt_updated_at`),
  MODIFY `ltt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `list_type_transaction` (`ltt_id`, `ltt_ref`, `ltt_pl`, `ltt_en`, `ltt_created_at`, `ltt_updated_at`) VALUES
  (1, 'sale', 'sprzedaż', 'sale', '2020-11-25 15:01:42', '2021-05-15 13:20:55'),
  (2, 'purchase', 'zakup', 'purchase', '2020-11-25 15:05:13', '2020-12-03 11:06:05');

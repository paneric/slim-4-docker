CREATE TABLE `list_country` (
  `lc_id` int(11) UNSIGNED NOT NULL,
  `lc_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lc_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lc_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lc_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lc_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `list_country`
  ADD PRIMARY KEY (`lc_id`),
  ADD UNIQUE KEY `lc_ref` (`lc_ref`),
  ADD KEY `lc_created_at` (`lc_created_at`),
  ADD KEY `lc_updated_at` (`lc_updated_at`),
  MODIFY `lc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `list_country` (`lc_id`, `lc_ref`, `lc_pl`, `lc_en`, `lc_created_at`, `lc_updated_at`) VALUES
  (1, 'belgium', 'Belgia', 'Belgium', '2020-12-02 14:13:04', '2021-05-08 20:42:05'),
  (2, 'bulgaria', 'Bułgaria', 'Bulgaria', '2020-12-02 14:13:04', '2021-04-22 18:14:49'),
  (3, 'croatia', 'Chorwacja', 'Croatia', '2020-12-02 14:13:04', '2021-04-22 18:14:49'),
  (4, 'cyprus', 'Cypr', 'Cyprus', '2020-12-02 14:13:04', '2020-12-02 14:14:07'),
  (5, 'czech_republic', 'Republika Czeska', 'Chech Republic', '2020-12-02 14:13:04', '2020-12-02 15:28:41'),
  (6, 'denmark', 'Dania', 'Denmark', '2020-12-02 14:13:04', '2021-05-05 18:30:35'),
  (7, 'estonia', 'Estonia', 'Estonia', '2020-12-02 14:13:04', '2021-05-05 18:30:35'),
  (8, 'finland', 'Finlandia', 'Finland', '2020-12-02 14:13:04', '2021-05-05 18:30:35'),
  (9, 'france', 'Francja', 'France', '2020-12-02 14:13:04', '2021-05-05 18:30:35'),
  (10, 'germany', 'Niemcy', 'Germany', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (11, 'grece', 'Grecja', 'Grece', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (12, 'hungary', 'Węgry', 'Hungary', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (13, 'ireland', 'Irlandia', 'Ireland', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (14, 'italy', 'Włochy', 'Italy', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (15, 'latvia', 'Łotwa', 'Latvia', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (16, 'lithuania', 'Litwa', 'Lithuania', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (17, 'luxembourg', 'Luksemburg', 'Luxembourg', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (18, 'malta', 'Malta', 'Malta', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
  (19, 'holland', 'Holandia', 'Holland', '2020-12-02 14:20:39', '2020-12-07 10:49:46'),
  (20, 'poland', 'Polska', 'Poland', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
  (21, 'portugal', 'Portugalia', 'Portugal', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
  (22, 'romania', 'Rumunia', 'Romania', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
  (23, 'slovakia', 'Słowacja', 'Slovakia', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
  (24, 'slovenia', 'Słowenia', 'Slovenia', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
  (25, 'spain', 'Hiszpania', 'Spain', '2020-12-02 14:23:52', '2020-12-02 14:23:52');

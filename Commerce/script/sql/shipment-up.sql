CREATE TABLE `shipment` (
  `shp_id` int(11) UNSIGNED NOT NULL,
  `shp_ref` varchar(255) COLLATE utf8_unicode_ci,
  `shp_list_type_shipment_id` int(11) UNSIGNED NOT NULL,
  `shp_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `shp_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `shp_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `shipment`
  ADD PRIMARY KEY (`shp_id`),
  ADD UNIQUE KEY `shp_ref` (`shp_ref`),
  ADD KEY `shp_list_type_shipment_id` (`shp_list_type_shipment_id`) USING BTREE,
  ADD KEY `shp_date` (`shp_date`),
  ADD KEY `shp_created_at` (`shp_created_at`),
  ADD KEY `shp_updated_at` (`shp_updated_at`);

ALTER TABLE `shipment`
  MODIFY `shp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `shipment`
  ADD CONSTRAINT `shipment_ibfk_1` FOREIGN KEY (`shp_list_type_shipment_id`) REFERENCES `list_type_shipment` (`lts_id`);

INSERT INTO `shipment` (`shp_ref`, `shp_list_type_shipment_id`, `shp_date`) VALUES
  ('ship1', '1', '2020-12-04 22:24:39'),
  ('ship2', '2', '2020-12-05 18:14:50'),
  ('ship3', '2', '2020-12-05 19:21:59'),
  ('ship4', '3', '2020-12-05 19:23:59'),
  ('ship5', '4', '2020-12-06 01:55:47');

CREATE TABLE `company` (
  `cmp_id` int(11) UNSIGNED NOT NULL,
  `cmp_list_type_company_id` int(11) UNSIGNED NOT NULL,
  `cmp_ref` varchar(255) COLLATE utf8_unicode_ci,
  `cmp_name` varchar(255) COLLATE utf8_unicode_ci,
  `cmp_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cmp_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `company`
  ADD PRIMARY KEY (`cmp_id`),
  ADD UNIQUE KEY `cmp_ref` (`cmp_ref`),
  ADD UNIQUE KEY `cmp_name` (`cmp_name`),
  ADD KEY `cmp_list_type_company_id` (`cmp_list_type_company_id`) USING BTREE,
  ADD KEY `cmp_created_at` (`cmp_created_at`),
  ADD KEY `cmp_updated_at` (`cmp_updated_at`),
  MODIFY `cmp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1,
  ADD CONSTRAINT `company_ibfk_2` FOREIGN KEY (`cmp_list_type_company_id`) REFERENCES `list_type_company` (`ltc_id`);

INSERT INTO `company` (`cmp_id`, `cmp_list_type_company_id`, `cmp_ref`, `cmp_name`) VALUES
  (1, 1, NULL, NULL),
  (2, 7, 'company_ref_1', 'company_name_1'),
  (3, 8, 'company_ref_2', 'company_name_2');

<?php

declare(strict_types=1);

namespace Paneric\Commerce\User\config;

use Paneric\Commerce\User\Model\UserDAO;
use Paneric\Commerce\User\Model\UserVLD;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'user';
    protected string $prefix = 'usr_';

    protected string $uniqueKey = 'ref';//unique key

    protected bool $stringId = true;

    protected string $daoClass = UserDAO::class;
    protected string $vldClass = UserVLD::class;
}

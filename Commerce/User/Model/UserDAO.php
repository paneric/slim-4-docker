<?php

declare(strict_types=1);

namespace Paneric\Commerce\User\Model;

use Paneric\DataObject\DAO;

class UserDAO extends DAO
{
    use UserTrait;

    protected ?string $id;

    protected string $firstName;
    protected string $lastName;

    public function __construct()
    {
        $this->prefix = 'usr_';

        $this->setMaps();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }
    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }
}

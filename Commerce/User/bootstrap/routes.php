<?php

declare(strict_types=1);

use Paneric\ComponentModule\Action\CreateApiAction;
use Paneric\ComponentModule\Action\CreateMultipleApiAction;
use Paneric\ComponentModule\Action\DeleteApiAction;
use Paneric\ComponentModule\Action\DeleteMultipleApiAction;
use Paneric\ComponentModule\Action\GetAllApiAction;
use Paneric\ComponentModule\Action\GetAllByApiAction;
use Paneric\ComponentModule\Action\GetAllByExtendedApiAction;
use Paneric\ComponentModule\Action\GetOneByApiAction;
use Paneric\ComponentModule\Action\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Action\GetOneByIdApiAction;
use Paneric\ComponentModule\Action\UpdateApiAction;
use Paneric\ComponentModule\Action\UpdateMultipleApiAction;
use Paneric\ComponentModule\Controller\ApiModuleController;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container)) {
    try {
        $slim->post('/api/user/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApiAction::class)
            );
        })->setName('api-user.add');

        $slim->post('/api/users/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApiAction::class)
            );
        })->setName('api-users.add');


        $slim->delete('/api/user/remove/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->delete(
                $request,
                $response,
                $this->get(DeleteApiAction::class),
                $args['id']
            );
        })->setName('api-user.remove');

        $slim->post('/api/users/remove', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(DeleteMultipleApiAction::class)
            );
        })->setName('api-users.remove');


        $slim->get('/api/users/get', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApiAction::class)
            );
        })->setName('api-users.get');

        $slim->post('/api/users/get-ext', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApiAction::class)
            );
        })->setName('api-users.get-ext');

        $slim->get('/api/users/get/{field}/{value}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName('api-users.get-by');

        $slim->get(
            '/api/users/get-paginated[/{page}]',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getAllPaginated(
                    $request,
                    $response,
                    $this->get(GetAllPaginatedApiAction::class),
                    $args['page'] ?? 1
                );
            }
        )->setName('api-users.get-paginated')
            ->addMiddleware($container->get(PaginationMiddleware::class));


        $slim->get('/api/user/get/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApiAction::class),
                $args['id']
            );
        })->setName('api-user.get-by-id');

        $slim->get(
            '/api/user/get/{field}/{value}',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getOneBy(
                    $request,
                    $response,
                    $this->get(GetOneByApiAction::class),
                    $args['field'],
                    $args['value']
                );
            }
        )->setName('api-user.get-by');


        $slim->put('/api/user/edit/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->update(
                $request,
                $response,
                $this->get(UpdateApiAction::class),
                $args['id']
            );
        })->setName('api-user.edit');

        $slim->put('/api/users/edit', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(UpdateMultipleApiAction::class)
            );
        })->setName('api-users.edit');
    } catch (Exception $e) {
    }
}

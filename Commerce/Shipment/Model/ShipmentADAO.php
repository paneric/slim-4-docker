<?php

declare(strict_types=1);

namespace Paneric\Commerce\Shipment\Model;

use Paneric\Commerce\ListTypeShipment\Model\ListTypeShipmentDAO;
use Paneric\DataObject\ADAO;

class ShipmentADAO extends ADAO
{
    use ShipmentTrait;

    protected ?int $id;

    protected ?ListTypeShipmentDAO $listTypeShipment;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'shp_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->listTypeShipment = new ListTypeShipmentDAO();
            $this->values = $this->listTypeShipment->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListTypeShipment(): ?ListTypeShipmentDAO
    {
        return $this->listTypeShipment;
    }


    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setListTypeShipment(?ListTypeShipmentDAO $listTypeShipment): void
    {
        $this->listTypeShipment = $listTypeShipment;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Shipment\Model;

use Paneric\DataObject\DAO;

class ShipmentDAO extends DAO
{
    use ShipmentTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'shp_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Shipment\Model;

trait ShipmentTrait
{
    protected ?int $listTypeShipmentId;

    protected string $ref;
    protected string $date;


    public function getListTypeShipmentId(): ?int
    {
        return $this->listTypeShipmentId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function getDate(): string
    {
        return $this->date;
    }


    public function setListTypeShipmentId(int $listTypeShipmentId): void
    {
        $this->listTypeShipmentId = $listTypeShipmentId;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setDate(string $date): void
    {
        $this->date = $date;
    }
}

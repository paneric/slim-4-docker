<?php

declare(strict_types=1);

namespace Paneric\Commerce\Shipment\config;

use Paneric\Commerce\Shipment\Model\ShipmentADAO;
use Paneric\Commerce\Shipment\Model\ShipmentDAO;
use Paneric\Commerce\Shipment\Model\ShipmentVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'shipment';
    protected string $prefix = 'shp_';

    protected string $left = 'ref';
    protected string $prefixLeft = '';

    protected string $right = 'ref';
    protected string $prefixRight = '';

    protected string $uniqueKey = 'ref';//unique key

    protected bool $stringId = false;

    protected string $adaoClass = ShipmentADAO::class;
    protected string $daoClass = ShipmentDAO::class;
    protected string $vldClass = ShipmentVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM shipment AS shp
                        LEFT JOIN list_type_shipment AS lts ON shp.shp_list_type_shipment_id = lts.lts_id
                ";
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\AddressShipment\Model;

use Paneric\Commerce\Address\Model\AddressDAO;
use Paneric\Commerce\Shipment\Model\ShipmentDAO;
use Paneric\DataObject\ADAO;

class AddressShipmentADAO extends ADAO
{
    use AddressShipmentTrait;

    protected ?int $id;

    protected ?AddressDAO $address;
    protected ?ShipmentDAO $shipment;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'adsh_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->address = new AddressDAO();
            $this->values = $this->address->hydrate($this->values);

            $this->shipment = new ShipmentDAO();
            $this->values = $this->shipment->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?AddressDAO
    {
        return $this->address;
    }
    public function getShipment(): ?ShipmentDAO
    {
        return $this->shipment;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setAddress(?AddressDAO $address): void
    {
        $this->address = $address;
    }
    public function setShipment(?ShipmentDAO $shipment): void
    {
        $this->shipment = $shipment;
    }
}

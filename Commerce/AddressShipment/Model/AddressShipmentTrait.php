<?php

declare(strict_types=1);

namespace Paneric\Commerce\AddressShipment\Model;

trait AddressShipmentTrait
{
    protected ?int $addressId;
    protected ?int $shipmentId;


    public function getAddressId(): ?int
    {
        return $this->addressId;
    }
    public function getShipmentId(): ?int
    {
        return $this->shipmentId;
    }


    public function setAddressId(?int $addressId): void
    {
        $this->addressId = $addressId;
    }
    public function setShipmentId(?int $shipmentId): void
    {
        $this->shipmentId = $shipmentId;
    }
}

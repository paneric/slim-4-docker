<?php

declare(strict_types=1);

namespace Paneric\Commerce\AddressShipment\config;

use Paneric\Commerce\AddressShipment\Model\AddressShipmentADAO;
use Paneric\Commerce\AddressShipment\Model\AddressShipmentDAO;
use Paneric\Commerce\AddressShipment\Model\AddressShipmentVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'address_shipment';
    protected string $prefix = 'adsh_';

    protected string $left = 'address';
    protected string $prefixLeft = 'adr_';

    protected string $right = 'shipment';
    protected string $prefixRight = 'shp_';

    protected bool $stringId = false;

    protected string $uniqueKey = 'shipment_id';//unique key

    protected string $adaoClass = AddressShipmentADAO::class;
    protected string $daoClass = AddressShipmentDAO::class;
    protected string $vldClass = AddressShipmentVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM address_shipment AS adsh
                        LEFT JOIN address AS adr ON adsh.adsh_address_id = adr.adr_id
                        LEFT JOIN shipment AS shp ON adsh.adsh_shipment_id = shp.shp_id
                ";
}

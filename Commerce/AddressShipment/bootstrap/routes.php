<?php

declare(strict_types=1);

use Paneric\ComponentModule\Action\CreateApiAction;
use Paneric\ComponentModule\Action\CreateMultipleApiAction;
use Paneric\ComponentModule\Action\DeleteApiAction;
use Paneric\ComponentModule\Action\DeleteMultipleApiAction;
use Paneric\ComponentModule\Action\GetAllApiAction;
use Paneric\ComponentModule\Action\GetAllByApiAction;
use Paneric\ComponentModule\Action\GetAllByExtendedApiAction;
use Paneric\ComponentModule\Action\GetOneByApiAction;
use Paneric\ComponentModule\Action\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Action\GetOneByIdApiAction;
use Paneric\ComponentModule\Action\UpdateApiAction;
use Paneric\ComponentModule\Action\UpdateMultipleApiAction;
use Paneric\ComponentModule\Controller\ApiModuleController;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container)) {
    try {
        $slim->post('/api/address-shipment/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApiAction::class)
            );
        })->setName('api-address-shipment.add');

        $slim->post('/api/address-shipments/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApiAction::class)
            );
        })->setName('api-address-shipments.add');


        $slim->delete('/api/address-shipment/remove/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->delete(
                $request,
                $response,
                $this->get(DeleteApiAction::class),
                $args['id']
            );
        })->setName('api-address-shipment.remove');

        $slim->post('/api/address-shipments/remove', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(DeleteMultipleApiAction::class)
            );
        })->setName('api-address-shipments.remove');


        $slim->get('/api/address-shipments/get', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApiAction::class)
            );
        })->setName('api-address-shipments.get');

        $slim->post('/api/address-shipments/get-ext', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApiAction::class)
            );
        })->setName('api-address-shipments.get-ext');

        $slim->get('/api/address-shipments/get/{field}/{value}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName('api-address-shipments.get-by');

        $slim->get(
            '/api/address-shipments/get-paginated[/{page}]',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getAllPaginated(
                    $request,
                    $response,
                    $this->get(GetAllPaginatedApiAction::class),
                    $args['page'] ?? 1
                );
            }
        )->setName('api-address-shipments.get-paginated')
            ->addMiddleware($container->get(PaginationMiddleware::class));


        $slim->get('/api/address-shipment/get/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApiAction::class),
                $args['id']
            );
        })->setName('api-address-shipment.get-by-id');

        $slim->get(
            '/api/address-shipment/get/{field}/{value}',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getOneBy(
                    $request,
                    $response,
                    $this->get(GetOneByApiAction::class),
                    $args['field'],
                    $args['value']
                );
            }
        )->setName('api-address-shipment.get-by');


        $slim->put('/api/address-shipment/edit/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->update(
                $request,
                $response,
                $this->get(UpdateApiAction::class),
                $args['id']
            );
        })->setName('api-address-shipment.edit');

        $slim->put('/api/address-shipments/edit', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(UpdateMultipleApiAction::class)
            );
        })->setName('api-address-shipments.edit');
    } catch (Exception $e) {
    }
}

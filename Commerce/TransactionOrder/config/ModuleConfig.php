<?php

declare(strict_types=1);

namespace Paneric\Commerce\TransactionOrder\config;

use Paneric\Commerce\TransactionOrder\Model\TransactionOrderADAO;
use Paneric\Commerce\TransactionOrder\Model\TransactionOrderDAO;
use Paneric\Commerce\TransactionOrder\Model\TransactionOrderVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'transaction_order';
    protected string $prefix = 'tror_';

    protected string $left = 'transaction';
    protected string $prefixLeft = 'trn_';

    protected string $right = 'orders';
    protected string $prefixRight = 'ord_';

    protected bool $stringId = false;

    protected string $uniqueKey = '';//unique key

    protected string $adaoClass = TransactionOrderADAO::class;
    protected string $daoClass = TransactionOrderDAO::class;
    protected string $vldClass = TransactionOrderVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM transaction_order AS tror
                        LEFT JOIN transaction AS trn ON tror.tror_transaction_id = trn.trn_id
                        LEFT JOIN orders AS ord ON tror.tror_order_id = ord.ord_id
                ";
}

<?php

declare(strict_types=1);

use Paneric\ComponentModule\Action\CreateApiAction;
use Paneric\ComponentModule\Action\CreateMultipleApiAction;
use Paneric\ComponentModule\Action\DeleteApiAction;
use Paneric\ComponentModule\Action\DeleteMultipleApiAction;
use Paneric\ComponentModule\Action\GetAllApiAction;
use Paneric\ComponentModule\Action\GetAllByApiAction;
use Paneric\ComponentModule\Action\GetAllByExtendedApiAction;
use Paneric\ComponentModule\Action\GetOneByApiAction;
use Paneric\ComponentModule\Action\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Action\GetOneByIdApiAction;
use Paneric\ComponentModule\Action\UpdateApiAction;
use Paneric\ComponentModule\Action\UpdateMultipleApiAction;
use Paneric\ComponentModule\Controller\ApiModuleController;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container)) {
    try {
        $slim->post('/api/transaction-order/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApiAction::class)
            );
        })->setName('api-transaction-order.add');

        $slim->post('/api/transaction-orders/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApiAction::class)
            );
        })->setName('api-transaction-orders.add');


        $slim->delete('/api/transaction-order/remove/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->delete(
                $request,
                $response,
                $this->get(DeleteApiAction::class),
                $args['id']
            );
        })->setName('api-transaction-order.remove');

        $slim->post('/api/transaction-orders/remove', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(DeleteMultipleApiAction::class)
            );
        })->setName('api-transaction-orders.remove');


        $slim->get('/api/transaction-orders/get', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApiAction::class)
            );
        })->setName('api-transaction-orders.get');

        $slim->post('/api/transaction-orders/get-ext', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApiAction::class)
            );
        })->setName('api-transaction-orders.get-ext');

        $slim->get('/api/transaction-orders/get/{field}/{value}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName('api-transaction-orders.get-by');

        $slim->get(
            '/transaction-orders/get-paginated[/{page}]',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getAllPaginated(
                    $request,
                    $response,
                    $this->get(GetAllPaginatedApiAction::class),
                    $args['page'] ?? 1
                );
            }
        )->setName('api-transaction-orders.get-paginated')
            ->addMiddleware($container->get(PaginationMiddleware::class));


        $slim->get('/api/transaction-order/get/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApiAction::class),
                $args['id']
            );
        })->setName('api-transaction-order.get-by-id');

        $slim->get(
            '/transaction-order/get/{field}/{value}',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getOneBy(
                    $request,
                    $response,
                    $this->get(GetOneByApiAction::class),
                    $args['field'],
                    $args['value']
                );
            }
        )->setName('api-transaction-order.get-by');


        $slim->put('/api/transaction-order/edit/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->update(
                $request,
                $response,
                $this->get(UpdateApiAction::class),
                $args['id']
            );
        })->setName('api-transaction-order.edit');

        $slim->put('/transaction-orders/edit', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(UpdateMultipleApiAction::class)
            );
        })->setName('api-transaction-orders.edit');
    } catch (Exception $e) {
    }
}

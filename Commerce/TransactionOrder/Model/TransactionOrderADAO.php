<?php

declare(strict_types=1);

namespace Paneric\Commerce\TransactionOrder\Model;

use Paneric\Commerce\Order\Model\OrderDAO;
use Paneric\Commerce\Transaction\Model\TransactionDAO;
use Paneric\DataObject\ADAO;

class TransactionOrderADAO extends ADAO
{
    use TransactionOrderTrait;

    protected ?int $id;

    protected ?TransactionDAO $transaction;
    protected ?OrderDAO $order;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'tror_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->transaction = new TransactionDAO();
            $this->values = $this->transaction->hydrate($this->values);

            $this->order = new OrderDAO();
            $this->values = $this->order->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTransaction(): ?TransactionDAO
    {
        return $this->transaction;
    }
    public function getOrder(): ?OrderDAO
    {
        return $this->order;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setTransaction(?TransactionDAO $transaction): void
    {
        $this->transaction = $transaction;
    }
    public function setOrder(?OrderDAO $order): void
    {
        $this->order = $order;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\TransactionOrder\Model;

trait TransactionOrderTrait
{
    protected ?int $transactionId;
    protected ?int $orderId;


    public function getTransactionId(): ?int
    {
        return $this->transactionId;
    }
    public function getOrderId(): ?int
    {
        return $this->orderId;
    }


    public function setTransactionId(?int $transactionId): void
    {
        $this->transactionId = $transactionId;
    }
    public function setOrderId(?int $orderId): void
    {
        $this->orderId = $orderId;
    }
}

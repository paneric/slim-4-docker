<?php

declare(strict_types=1);

namespace Paneric\Commerce\TransactionOrder\Model;

use Paneric\DataObject\DAO;

class TransactionOrderDAO extends DAO
{
    use TransactionOrderTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'tror_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

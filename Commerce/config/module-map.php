<?php

declare(strict_types=1);

return [
    'address' => './../Commerce/Address',
    'addresss' => './../Commerce/Address',
    'address-shipment' => './../Commerce/AddressShipment',
    'address-shipments' => './../Commerce/AddressShipment',
    'company' => './../Commerce/Company',
    'companys' => './../Commerce/Company',

    'list-country' => './../Commerce/ListCountry',
    'list-countrys' => './../Commerce/ListCountry',
    'list-status-order' => './../Commerce/ListStatusOrder',
    'list-status-orders' => './../Commerce/ListStatusOrder',
    'list-type-company' => './../Commerce/ListTypeCompany',
    'list-type-companys' => './../Commerce/ListTypeCompany',
    'list-type-payment' => './../Commerce/ListTypePayment',
    'list-type-payments' => './../Commerce/ListTypePayment',
    'list-type-shipment' => './../Commerce/ListTypeShipment',
    'list-type-shipments' => './../Commerce/ListTypeShipment',
    'list-type-transaction' => './../Commerce/ListTypeTransaction',
    'list-type-transactions' => './../Commerce/ListTypeTransaction',
    'order' => './../Commerce/Order',
    'orders' => './../Commerce/Order',
    'order-payment' => './../Commerce/OrderPayment',
    'order-payments' => './../Commerce/OrderPayment',
    'payment' => './../Commerce/Payment',
    'payments' => './../Commerce/Payment',

    'product' => './../PIM/Product',
    'products' => './../PIM/Product',

    'shipment' => './../Commerce/Shipment',
    'shipments' => './../Commerce/Shipment',
    'shipment-transaction' => './../Commerce/ShipmentTransaction',
    'transaction' => './../Commerce/Transaction',
    'shipment-transactions' => './../Commerce/ShipmentTransaction',
    'transactions' => './../Commerce/Transaction',
    'transaction-order' => './../Commerce/TransactionOrder',
    'transaction-orders' => './../Commerce/TransactionOrder',
    'user' => './../Commerce/User',
    'users' => './../Commerce/User',
    'user-address' => './../src/Commerce/UserAddress',
    'user-addresss' => './../src/Commerce/UserAddress',
];

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Payment\Model;

use Paneric\Commerce\ListTypePayment\Model\ListTypePaymentDAO;
use Paneric\DataObject\ADAO;

class PaymentADAO extends ADAO
{
    use PaymentTrait;

    protected ?int $id;

    protected ?ListTypePaymentDAO $listTypePayment;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'pay_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->listTypePayment = new ListTypePaymentDAO();
            $this->values = $this->listTypePayment->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListTypePayment(): ?ListTypePaymentDAO
    {
        return $this->listTypePayment;
    }


    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setListTypePayment(?ListTypePaymentDAO $listTypePayment): void
    {
        $this->listTypePayment = $listTypePayment;
    }
}

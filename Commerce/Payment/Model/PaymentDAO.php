<?php

declare(strict_types=1);

namespace Paneric\Commerce\Payment\Model;

use Paneric\DataObject\DAO;

class PaymentDAO extends DAO
{
    use PaymentTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'pay_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Payment\Model;

trait PaymentTrait
{
    protected ?int $listTypePaymentId;

    protected string $ref;
    protected int $amount;
    protected ?string $communication;
    protected ?string $remarks;
    protected ?string $date;


    public function getListTypePaymentId(): ?int
    {
        return $this->listTypePaymentId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function getAmount(): int
    {
        return $this->amount;
    }
    public function getCommunication(): ?string
    {
        return $this->communication;
    }
    public function getRemarks(): ?string
    {
        return $this->remarks;
    }
    public function getDate(): string
    {
        return $this->date;
    }


    public function setListTypePaymentId(int $listTypePaymentId): void
    {
        $this->listTypePaymentId = $listTypePaymentId;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }
    public function setCommunication(?string $communication): void
    {
        $this->communication = $communication;
    }
    public function setRemarks(?string $remarks): void
    {
        $this->remarks = $remarks;
    }
    public function setDate(string $date): void
    {
        $this->date = $date;
    }
}

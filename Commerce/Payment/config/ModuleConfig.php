<?php

declare(strict_types=1);

namespace Paneric\Commerce\Payment\config;

use Paneric\Commerce\Payment\Model\PaymentADAO;
use Paneric\Commerce\Payment\Model\PaymentDAO;
use Paneric\Commerce\Payment\Model\PaymentVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'payment';
    protected string $prefix = 'pay_';

    protected string $left = 'ref';
    protected string $prefixLeft = '';

    protected string $right = 'ref';
    protected string $prefixRight = '';

    protected string $uniqueKey = 'ref';//unique key

    protected bool $stringId = false;

    protected string $adaoClass = PaymentADAO::class;
    protected string $daoClass = PaymentDAO::class;
    protected string $vldClass = PaymentVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM payment AS pay
                        LEFT JOIN list_type_payment AS ltp ON pay.pay_list_type_payment_id = ltp.ltp_id
                ";
}

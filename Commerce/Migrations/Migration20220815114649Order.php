<?php

declare(strict_types=1);

namespace Paneric\Commerce\Migrations;

use Exception;
use Paneric\Migrations\AbstractMigration;

final class Migration20220815114649Order extends AbstractMigration
{
    private const SQL_PATH = '';

    public function getDescription(): string
    {
        return '';
    }

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        $this->executeQuery(file_get_contents(self::SQL_PATH));
    }
}

<?php

declare(strict_types=1);

namespace Paneric\Commerce\Address\config;

use JetBrains\PhpStorm\ArrayShape;
use Paneric\Commerce\Address\Model\AddressADAO;
use Paneric\Commerce\Address\Model\AddressDAO;
use Paneric\Commerce\Address\Model\AddressVLD;
use Paneric\ComponentModule\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use PDO;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'address';
    protected string $prefix = 'adr_';

    protected string $left = '';
    protected string $prefixLeft = '';

    protected string $right = '';
    protected string $prefixRight = '';

    protected string $uniqueKey = '';//unique key

    protected bool $stringId = false;

    protected string $adaoClass = AddressADAO::class;
    protected string $daoClass = AddressDAO::class;
    protected string $vldClass = AddressVLD::class;

    protected string $query = "
                    SELECT * 
                    FROM address AS mt
                        LEFT JOIN list_country AS stl ON mt.adr_list_country_id = stl.lc_id
                        LEFT JOIN company AS str ON mt.adr_company_id = str.cmp_id
                ";

    #[ArrayShape([
        'table' => "string",
        'dto_class' => "string",
        'fetch_mode' => "int",
        'create_unique_where' => "string",
        'update_unique_where' => "string"
    ])]
    public function persister(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->daoClass,
            'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            'create_unique_where' => '',
            'update_unique_where' => '',
        ];
    }
}

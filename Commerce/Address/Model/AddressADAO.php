<?php

declare(strict_types=1);

namespace Paneric\Commerce\Address\Model;

use Paneric\Commerce\Company\Model\CompanyDAO;
use Paneric\Commerce\ListCountry\Model\ListCountryDAO;
use Paneric\DataObject\ADAO;

class AddressADAO extends ADAO
{
    use AddressTrait;

    protected ?int $id;

    protected ?ListCountryDAO $listCountry;
    protected ?CompanyDAO $company;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'adr_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->listCountry = new ListCountryDAO();
            $this->values = $this->listCountry->hydrate($this->values);

            $this->company = new CompanyDAO();
            $this->values = $this->company->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListCountry(): ?ListCountryDAO
    {
        return $this->listCountry;
    }
    public function getCompanyl(): ?CompanyDAO
    {
        return $this->company;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setListCountry(?ListCountryDAO $listCountry): void
    {
        $this->listCountry = $listCountry;
    }
    public function setCompany(?CompanyDAO $company): void
    {
        $this->company = $company;
    }
}

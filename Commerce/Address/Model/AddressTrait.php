<?php

declare(strict_types=1);

namespace Paneric\Commerce\Address\Model;

trait AddressTrait
{
    protected ?int $listCountryId;
    protected ?int $companyId;

    protected string $street;
    protected string $poNumber;
    protected string $poBox;
    protected string $city;
    protected string $poCode;

    public function getListCountryId(): ?int
    {
        return $this->listCountryId;
    }
    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    public function getStreet(): string
    {
        return $this->street;
    }
    public function getPoNumber(): string
    {
        return $this->poNumber;
    }
    public function getPoBox(): string
    {
        return $this->poBox;
    }
    public function getCity(): string
    {
        return $this->city;
    }
    public function getPoCode(): string
    {
        return $this->poCode;
    }


    public function setListCountryId(int $listCountryId): void
    {
        $this->listCountryId = $listCountryId;
    }
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }
    public function setPoNumber(string $poNumber): void
    {
        $this->poNumber = $poNumber;
    }
    public function setPoBox(string $poBox): void
    {
        $this->poBox = $poBox;
    }
    public function setCity(string $city): void
    {
        $this->city = $city;
    }
    public function setPoCode(string $poCode): void
    {
        $this->poCode = $poCode;
    }
}

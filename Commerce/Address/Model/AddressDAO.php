<?php

declare(strict_types=1);

namespace Paneric\Commerce\Address\Model;

use Paneric\DataObject\DAO;

class AddressDAO extends DAO
{
    use AddressTrait;

    protected ?int $id;

    public function __construct()
    {
        $this->prefix = 'adr_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}

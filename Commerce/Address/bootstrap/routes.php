<?php

declare(strict_types=1);

use Paneric\ComponentModule\Action\CreateApiAction;
use Paneric\ComponentModule\Action\CreateMultipleApiAction;
use Paneric\ComponentModule\Action\DeleteApiAction;
use Paneric\ComponentModule\Action\DeleteMultipleApiAction;
use Paneric\ComponentModule\Action\GetAllApiAction;
use Paneric\ComponentModule\Action\GetAllByApiAction;
use Paneric\ComponentModule\Action\GetAllByExtendedApiAction;
use Paneric\ComponentModule\Action\GetOneByApiAction;
use Paneric\ComponentModule\Action\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Action\GetOneByIdApiAction;
use Paneric\ComponentModule\Action\UpdateApiAction;
use Paneric\ComponentModule\Action\UpdateMultipleApiAction;
use Paneric\ComponentModule\Controller\ApiModuleController;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container)) {
    try {
        $slim->post('/api/address/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApiAction::class)
            );
        })->setName('api-address.add');

        $slim->post('/api/addresss/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApiAction::class)
            );
        })->setName('api-addresss.add');


        $slim->delete('/api/address/remove/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->delete(
                $request,
                $response,
                $this->get(DeleteApiAction::class),
                $args['id']
            );
        })->setName('api-address.remove');

        $slim->post('/api/addresss/remove', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(DeleteMultipleApiAction::class)
            );
        })->setName('api-addresss.remove');


        $slim->get('/api/addresss/get', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApiAction::class)
            );
        })->setName('api-addresss.get');

        $slim->post('/api/addresss/get-ext', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApiAction::class)
            );
        })->setName('api-addresss.get-ext');

        $slim->get('/api/addresss/get/{field}/{value}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName('api-addresss.get-by');

        $slim->get(
            '/api/addresss/get-paginated[/{page}]',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getAllPaginated(
                    $request,
                    $response,
                    $this->get(GetAllPaginatedApiAction::class),
                    $args['page'] ?? 1
                );
            }
        )->setName('api-addresss.get-paginated')
            ->addMiddleware($container->get(PaginationMiddleware::class));


        $slim->get('/api/address/get/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApiAction::class),
                $args['id']
            );
        })->setName('api-address.get-by-id');

        $slim->get(
            '/api/address/get/{field}/{value}',
            function (Request $request, Response $response, array $args) {
                return $this->get(ApiModuleController::class)->getOneBy(
                    $request,
                    $response,
                    $this->get(GetOneByApiAction::class),
                    $args['field'],
                    $args['value']
                );
            }
        )->setName('api-address.get-by');


        $slim->put('/api/address/edit/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->update(
                $request,
                $response,
                $this->get(UpdateApiAction::class),
                $args['id']
            );
        })->setName('api-address.edit');

        $slim->put('/api/addresss/edit', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(UpdateMultipleApiAction::class)
            );
        })->setName('api-addresss.edit');
    } catch (Exception $e) {
    }
}

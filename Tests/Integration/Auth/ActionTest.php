<?php

declare(strict_types=1);

namespace Tests\Integration\Auth;

use PHPUnit\Framework\TestCase;
use Tests\Integration\Auth\Traits\DtoTestTrait;
use Tests\Integration\Auth\Traits\TestTrait;

class ActionTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    public function getAllActionProvider(): array
    {
        return [
            [
                'http://localhost/api/actions/get',
            ],
        ];
    }

    public function getOneByIdActionProvider(): array
    {
        return [
            [
                'http://localhost/api/action/get/%s',
                '61cce41c2eb9b5.59298252',
                'x.x',
                'resource_not_found',
            ],
        ];
    }

    public function addActionProvider(): array
    {
        return [
            [
                'http://localhost/api/action/add',
                [
                    'ref' => 'action_test_add',
                ],
                'http://localhost/api/action/get/%s/%s',
                'ref',
                'action_test_add',
                'db_add_unique_error',
                'http://localhost/api/action/remove/%s',
            ],
        ];
    }

    public function addActionsProvider(): array
    {
        return [
            [
                'http://localhost/api/actions/add',
                [
                    'ref' => [
                        'action_test_add_1',
                        'action_test_add_2',
                    ],
                ],
                [
                    'ref' => [
                        'action_test_add_2',
                        'action_test_add_3',
                    ],
                ],
                'ref',
                'action_test_add_2',
                'http://localhost/api/action/get/%s/%s',
                'http://localhost/api/action/remove/%s',
                [
                    'action_test_add_1',
                    'action_test_add_2',
                    'action_test_add_3',
                ],
            ],
        ];
    }

    public function updateActionProvider(): array
    {
        return [
            [
                'http://localhost/api/action/add',
                [
                    'ref' => 'action_test_add_1',
                ],
                [
                    'ref' => 'action_test_add_2',
                ],
                'http://localhost/api/action/get/%s/%s',
                'ref',
                'action_test_add_1',
                'http://localhost/api/action/edit/%s',
                [
                    'ref' => 'action_test_update',
                ],
                'db_update_unique_error',
                'action_test_add_2',
                'http://localhost/api/action/remove/%s',
            ],
        ];
    }

    public function updateActionsProvider(): array
    {
        return [
            [
                'http://localhost/api/actions/add',
                [
                    'ref' => [
                        'action_test_update_1',
                        'action_test_update_2',
                    ],
                ],
                'http://localhost/api/action/get/%s/%s',
                'ref',
                [
                    'action_test_update_1',
                    'action_test_update_2',
                ],
                'http://localhost/api/actions/edit',
                [
                    'ref' => [
                        'action_test_update_2',
                        'action_test_update_3',
                    ],
                ],
                'action_test_update_2',
                'http://localhost/api/action/remove/%s',
            ],
        ];
    }

    public function deleteActionsProvider(): array
    {
        return [
            [
                'http://localhost/api/actions/add',
                [
                    'ref' => [
                        'action_test_delete_1',
                        'action_test_delete_2',
                        'action_test_delete_3',
                    ],

                ],
                'http://localhost/api/action/get/%s/%s',
                'ref',
                [
                    'action_test_delete_1',
                    'action_test_delete_2',
                    'action_test_delete_3',
                ],
                'http://localhost/api/actions/remove',
            ],
        ];
    }
}

<?php

declare(strict_types=1);

namespace Tests\Integration\Auth\Traits;

use JsonException;

trait TestTrait
{
    /**
     * @throws JsonException
     */
    protected function getCurlResponse(string $method, string $url, array $payLoad = null): array
    {
        $conn = curl_init();
        curl_setopt($conn, CURLOPT_URL, $url);
        curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
        if ($payLoad !== null) {
            curl_setopt($conn, CURLOPT_POSTFIELDS, http_build_query($payLoad));
        }
        curl_setopt($conn, CURLOPT_CUSTOMREQUEST, $method);

        return json_decode(curl_exec($conn), true, 512, JSON_THROW_ON_ERROR);
    }
}

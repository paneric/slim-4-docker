<?php

declare(strict_types=1);

namespace Tests\Integration\Auth\Traits;

use JsonException;

trait DtoTestTrait
{
    /**
     * @dataProvider getAllActionProvider
     *
     * @throws JsonException
     */
    public function testIfGetsAll(string $url1): void
    {
        $response = $this->getCurlResponse('GET', $url1);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('body', $response);

        $this->assertEquals(200, $response['status']);
    }

    /**
     * @dataProvider getOneByIdActionProvider
     *
     * @throws JsonException
     */
    public function testIfGetsOneById(string $url1, string $id1, string $id2, string $error): void
    {
        $url = sprintf($url1, urlencode($id1));

        $response = $this->getCurlResponse('GET', $url);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('body', $response);

        $this->assertEquals(200, $response['status']);


        $url = sprintf($url1, urlencode($id2));

        $response = $this->getCurlResponse('GET', $url);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('error', $response);

        $this->assertEquals(400, $response['status']);
        $this->assertEquals($error, $response['error']);
    }

    /**
     * @dataProvider addActionProvider
     *
     * @throws JsonException
     */
    public function testIfAddsAction(
        string $url1,
        array $payLoad1,
        string $url2,
        string $field,
        string $fieldValue,
        string $error,
        string $url3
    ): void {
        $response = $this->getCurlResponse('POST', $url1, $payLoad1);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(201, $response['status']);


        $response = $this->getCurlResponse('POST', $url1, $payLoad1);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(400, $response['status']);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals($error, $response['error']);


        $url2 = sprintf($url2, $field, urlencode($fieldValue));

        $response = $this->getCurlResponse('GET', $url2);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);

        $this->assertArrayHasKey('body', $response);
        $this->assertEquals($fieldValue, $response['body'][$field]);


        $id = $response['body']['id'];

        $url3 = sprintf($url3, urlencode($id));

        $response = $this->getCurlResponse('DELETE', $url3);

        $this->assertEquals(200, $response['status']);
    }

    /**
     * @dataProvider addActionsProvider
     * @param string[] $fieldValues
     *
     * @throws JsonException
     */
    public function testIfAddsActions(
        string $url1,
        array $payLoad1,
        array $payLoad2,
        string $field,
        string $fieldValue2,
        string $url2,
        string $url3,
        array $fieldValues,
    ): void {
        $response = $this->getCurlResponse('POST', $url1, $payLoad1);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(201, $response['status']);

        $response = $this->getCurlResponse('POST', $url1, $payLoad2);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(400, $response['status']);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals('db_create_multiple_error', $response['error']);

        $this->assertArrayHasKey('body', $response);
        $this->assertEquals($fieldValue2, $response['body'][0][$field]);

        foreach ($fieldValues as $fieldValue) {
            $url = sprintf($url2, $field, urlencode($fieldValue));
            $response = $this->getCurlResponse('GET', $url);
            $id = $response['body']['id'];

            $url = sprintf($url3, urlencode($id));
            $this->getCurlResponse('DELETE', $url);
        }
    }

    /**
     * @dataProvider updateActionProvider
     *
     * @throws JsonException
     */
    public function testIfUpdatesAction(
        string $url1,
        array $payLoad1,
        array $payLoad2,
        string $url2,
        string $field,
        string $fieldValue1,
        string $url3,
        array $payLoad3,
        string $error,
        string $fieldValue2,
        string $url4
    ): void {
        $this->getCurlResponse('POST', $url1, $payLoad1);
        $this->getCurlResponse('POST', $url1, $payLoad2);


        $url = sprintf($url2, $field, urlencode($fieldValue1));

        $response = $this->getCurlResponse('GET', $url);

        $id1 = $response['body']['id'];


        $url = sprintf($url3, urlencode($id1));

        $response = $this->getCurlResponse('PUT', $url, $payLoad3);

        $this->assertEquals(200, $response['status']);


        $url = sprintf($url3, urlencode($id1));

        $response = $this->getCurlResponse('PUT', $url, $payLoad2);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals($error, $response['error']);


        $url = sprintf($url2, $field, urlencode($fieldValue2));

        $response = $this->getCurlResponse('GET', $url);

        $id2 = $response['body']['id'];


        $url = sprintf($url4, urlencode($id1));

        $this->getCurlResponse('DELETE', $url);

        $url = sprintf($url4, urlencode($id2));

        $this->getCurlResponse('DELETE', $url);
    }

    /**
     * @dataProvider updateActionsProvider
     *
     * @throws JsonException
     */
    public function testIfUpdatesActions(
        string $url1,
        array $payLoad1,
        string $url2,
        string $field,
        array $fieldValues,
        string $url3,
        array $payLoad2,
        string $fieldValue1,
        string $url4
    ): void {
        $this->getCurlResponse('POST', $url1, $payLoad1);

        $ids = [];
        foreach ($fieldValues as $fieldValue) {
            $url = sprintf($url2, $field, urlencode($fieldValue));
            $response = $this->getCurlResponse('GET', $url);
            $ids[] = $response['body']['id'];
        }

        $payLoad2['id'] = $ids;
        $response = $this->getCurlResponse('PUT', $url3, $payLoad2);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(400, $response['status']);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals('db_update_multiple_unique_error', $response['error']);

        $this->assertArrayHasKey('body', $response);
        $this->assertEquals($fieldValue1, $response['body'][0][$field]);

        foreach ($ids as $id) {
            $url = sprintf($url4, urlencode($id));
            $this->getCurlResponse('DELETE', $url);
        }
    }

    /**
     * @dataProvider deleteActionsProvider
     * @param string[] $fieldValues
     *
     * @throws JsonException
     */
    public function testIfDeletesActions(
        string $url1,
        array $payLoad1,
        string $url2,
        string $field,
        array $fieldValues,
        string $url3,
    ): void {
        $this->getCurlResponse('POST', $url1, $payLoad1);

        $ids = [];
        foreach ($fieldValues as $fieldValue) {
            $url = sprintf($url2, $field, urlencode($fieldValue));
            $response = $this->getCurlResponse('GET', $url);
            $ids[] = $response['body']['id'];
        }

        $payLoad['id'] = $ids;
        $response = $this->getCurlResponse('POST', $url3, $payLoad);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);


        $this->getCurlResponse('POST', $url1, $payLoad1);

        $ids = [];
        foreach ($fieldValues as $fieldValue) {
            $url = sprintf($url2, $field, urlencode($fieldValue));
            $response = $this->getCurlResponse('GET', $url);
            $ids[] = $response['body']['id'];
        }

        $ids[] = 'x.x';
        $payLoad['id'] = $ids;
        $response = $this->getCurlResponse('POST', $url3, $payLoad);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(400, $response['status']);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals('db_delete_multiple_error', $response['error']);

        $this->assertArrayHasKey('body', $response);
        $this->assertCount(1, $response['body']);
    }
}

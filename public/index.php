<?php

declare(strict_types=1);

use Dotenv\Dotenv;

// required by all routes.php and cam.php
define('ROOT_FOLDER', dirname(__DIR__) . '/');

require __DIR__ . '/../vendor/autoload.php';

$dotEnv = Dotenv::createImmutable(ROOT_FOLDER);
$dotEnv->load();
define('ENV', $_ENV['ENV']);

require './../src/bootstrap/bootstrap.php';

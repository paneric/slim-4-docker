<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleSdk\Request;

class CreateApcAction extends ApcAction
{
    public function invoke(): array
    {
        return [];
    }
}

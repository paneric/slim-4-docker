<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleSdk\Request;

class GetOneByApcAction extends ApcAction
{
    public function invoke(): array
    {
        return [];
    }
}

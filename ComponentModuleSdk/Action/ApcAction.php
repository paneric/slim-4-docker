<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleSdk\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Paneric\Logger\HttpClientLogger;

abstract class ApcAction
{
    public function __construct(protected Client $client, protected HttpClientLogger $httpClientLogger)
    {
    }

    public function getJsonResponse(string $method, string $url, array $options): array
    {
        try {
            $response = $this->client->request($method, $url, $options);
            $content = $response->getBody()->getContents();

            $status = $response->getStatusCode();

            if ($response->getHeaderLine('Content-Type') !== 'application/json;charset=utf-8') {
                $this->httpClientLogger->error(sprintf(
                    '%s %s %s %s',
                    'Method: ' . $method,
                    'Url: ' . $url,
                    'Options: ' . json_encode($options, JSON_THROW_ON_ERROR),
                    'Response: ' . $content,
                ));

                return [
                    'error' => 'Content type error',
                    'status' => $status
                ];
            }

            $response = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

            return array_merge($response, ['status' => $status]);
        } catch (GuzzleException | JsonException $e) {
            $this->httpClientLogger->error(sprintf(
                "%s%s%s%s",
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            ));
        }

        return [];
    }
}

<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleSdk\Responder;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

interface ApcModuleResponderInterface
{
    public function __invoke(Request $request, Response $response, array $data, ...$params): Response;
}

<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleSdk\Responder;

use JsonException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use RuntimeException;

class ApcModuleResponder implements ApcModuleResponderInterface
{
    public function __invoke(Request $request, Response $response, array $data, ...$params): Response
    {
        return $this->jsonResponse($response, $data, $params[0], $params[1] ?? 0);
    }

    protected function jsonResponse(
        Response $response,
        array $data = null,
        int $status = null,
        int $encodingOptions = 0
    ): Response {
        try {
            $json = false;

            $response->getBody()->write(
                $json = json_encode($data, JSON_THROW_ON_ERROR | $encodingOptions, 512)
            );
        } catch (JsonException $e) {
            echo sprintf(
                '%s%s%s%s',
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            );
        }

        if ($json === false) {
            throw new RuntimeException(json_last_error_msg(), json_last_error());
        }

        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');

        if (isset($status)) {
            return $response->withStatus($status);
        }

        return $response;
    }

    protected function htmlResponse(
        Response $response,
        array $data = null,
        int $status = null,
        int $encodingOptions = 0
    ): Response {
        return $response;
    }
}

### Export database
```shell
$ mysqldump -u [user] -p[password]`cat /etc/psa/.psa.shadow` [dbname] > [dbname.sql]
```
or zip:
```shell
$ mysqldump -u [user] -p[password] [db_name] | gzip > [dbname.sql.gz] 
```


### Import database
```shell
$ mysql -u [user] -p[password`cat /etc/psa/.psa.shadow` [dbname] < [dbname.sql]
```
or
```shell
$ mysql -u [user] -p[password`cat /etc/psa/.psa.shadow` dbname -e '[dbname.sql]'
```
or unzip
```shell
$ gunzip < [dbname.sql.gz]  | mysql -u [user] -p[password] [dbname] 
```


                if (!$stmt) {
                    echo "\nPDO::errorInfo():\n";
                    dd($this->pdo->errorInfo());
                    die;
                }

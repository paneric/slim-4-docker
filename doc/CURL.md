```php
function CallAPI($method, $api, $data) {
    $url = "http://localhost:82/slimdemo/RESTAPI/" . $api;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    
    switch ($method) {
        case "GET":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "POST":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
    }
    $response = curl_exec($curl);
    $data = json_decode($response);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Check the HTTP Status code
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            return ($data);
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    curl_close($curl);
    echo $error_status;
    die;
}
```



## Command line CURL requests examples

### 1. Fetching from database table.

#### 1.1 Fetching all items from database table:
```sh
$ curl -v http://slim-4-docker-nginx/actions/get
```

#### 1.2 Fetching all items from database table by field value:
```sh
$ curl -v http://slim-4-docker-nginx/actions/get/{field}/{value}
```

#### 1.3 Fetching all items paginated from database table:
```sh
$ curl -v http://slim-4-docker-nginx/actions/get-paginated[/{page}]
```

#### 1.4 Fetching one item by id from database table:
```sh
$ curl -v http://slim-4-docker-nginx/action/get/{id}
```

### 2. Adding to database table.

#### 2.1 Adding single new item:
```sh
$ curl -d '{"ref":"test1.*"}' -H 'Content-Type: application/json' -X POST http://slim-4-docker-nginx/action/add
```
#### 2.2 Adding multiple new items:
```sh
$ curl -d '{"ref":["test1.get","tests1.get"]}' -H 'Content-Type: application/json' -X POST http://slim-4-docker-nginx/actions/add
```

### 3 Altering items by `id` in database table:

#### 3.1 Altering single item:
```sh
$ curl -d '{"name":"test2.*"}' -H 'Content-Type: application/json' -X PUT http://slim-4-docker-nginx/action/edit/{id}
```
#### 3.2 Altering multiple items:
```sh
$ curl -d '{"id":["id1","id1s"],"ref":["test2.get","tests2.get"]}' -H 'Content-Type: application/json' -X PUT http://slim-4-docker-nginx/actions/edit/
```

### 4 Deleting items by `id` in from table:

#### 4.1 Deleting single item:
```sh
$ curl -X DELETE http://slim-4-docker-nginx/action/remove/{id}
```
#### 4.2 Deleting multiple items:
```sh
$ curl -d '{"id":["id1","id1s"]}' -H 'Content-Type: application/json' -X POST http://slim-4-docker-nginx/actions/remove
```










#### (2) Fetching products by "amount" field from data base:

(2.1) Fetching products by the **amount = 0**
```sh
$ curl -v http://slim-4-docker-nginx/api/products/show-by-amount/0
```
or:
```sh
$ curl -v http://slim-4-docker-nginx/api/products/show-by-amount/0/1
```
(2.2) Fetching products by the **amount > 0**
```sh
$ curl -v http://slim-4-docker-nginx/api/products/show-by-amount/0/2
```
(2.3) Fetching products by the **amount < 4**
```sh
$ curl -v http://slim-4-docker-nginx/api/products/show-by-amount/4/0
```

#### (3) Fetching product by _id = 1_ from data base:
```sh
$ curl -v http://slim-4-docker-nginx/api/product/1
```

#### (4) Adding new product:
```sh
$ curl -d '{"name":"Produkt 1","amount":4}' -H 'Content-Type: application/json' -X POST http://slim-4-docker-nginx/api/product
```

#### (5) Deleting product by _id = 1_ from data base:
```sh
$ curl -X DELETE http://slim-4-docker-nginx/api/product/1
```

#### (6) Altering product by _id = 1_ in data base: (all fields required)
```sh
$ curl -d '{"name":"Produkt 1","amount":12}' -H 'Content-Type: application/json' -X PUT http://slim-4-docker-nginx/api/product/1
```

#### (7) Updating product by _id = 1_ in data base:
```sh
$ curl -d '{"amount":8}' -H 'Content-Type: application/json' -X PATCH http://slim-4-docker-nginx/api/product/1
```
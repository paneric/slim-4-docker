https://blog.serverdensity.com/troubleshoot-nginx/

### 3. Virtual host domain

You need to double check you are using the right configuration in the virtual host. Make sure the `server_name` 
directive contains domains served by that virtual host:   

```
server {
    ...
    server_name example.com www.example.com;
    ...
}
```
```shell
$ /etc/nginx/sites-available directory
```

https://www.keycdn.com/support/nginx-virtual-host

```shell
$ vi /etc/hosts
```

```
# Virtual Hosts Local Test
1.2.3.4 testsite.com
```
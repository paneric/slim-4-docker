```shell
$ ./vendor/bin/phpcs --standard=PSR12 ./src -p --colors
$ ./vendor/bin/phpcbf --standard=PSR12 ./src -p --colors

$ ./vendor/bin/phpcs --standard=phpcs.xml ./src -p --colors
```

```
https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage

http://edorian.github.io/php-coding-standard-generator/#phpcs

https://pear.php.net/manual/en/package.php.php-codesniffer.annotated-ruleset.php
```
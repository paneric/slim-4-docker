## [base-module](https://bitbucket.org/paneric/base-module/src/master/)

```php
<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Apc;

use Paneric\CSRTriad\Action;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllApcAction extends Action
{
    protected $manager;

    protected $baseUrl;
    protected $uri;
    protected $moduleNameSc;
    protected $prefix;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['get_all'];

        $this->manager = $manager;

        $this->baseUrl = $configValues['base_url'];
        $this->uri = $configValues['api-lcs.get'];
        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request): array
    {
        $this->session->setFlash(
            ['module_name_sc' => $this->moduleNameSc],
            'value'
        );

        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $request->getAttribute('token'),
            ],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s',
                $this->baseUrl,
                $this->uri
            ),
            $options
        );

        if ($jsonResponse['status'] === 200) {
            return [
                $this->prefix . 's' => $jsonResponse['body']
            ];
        }

        return [];
    }
}
```

<?php

declare(strict_types=1);

namespace Paneric\PIM\Product\config;

use Paneric\PIM\Product\Model\ProductVLD;
use Paneric\PIM\Product\Model\ProductDAO;
use Paneric\ComponentModule\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'product';
    protected string $prefix = 'prd_';

    protected string $uniqueKey = 'ref';//unique key

    protected string $daoClass = ProductDAO::class;
    protected string $vldClass = ProductVLD::class;

    protected bool $stringId = false;
}

<?php

declare(strict_types=1);

namespace Paneric\PIM\Product\Model;

use Paneric\DataObject\DAO;

class ProductDAO extends DAO
{
    use ProductTrait;

    protected ?int $id;
    protected string $ref;

    public function __construct()
    {
        $this->prefix = 'prd_';

        $this->setMaps();
    }


    public function getId(): ?int
    {
        return $this->id;
    }
    public function getRef(): string
    {
        return $this->ref;
    }


    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
}

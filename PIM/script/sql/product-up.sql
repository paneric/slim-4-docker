CREATE TABLE `product` (
  `prd_id` int(11) UNSIGNED NOT NULL,
  `prd_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `product`
  ADD PRIMARY KEY (`prd_id`),
  ADD UNIQUE KEY `prd_ref` (`prd_ref`),
  MODIFY `prd_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `product` (`prd_id`, `prd_ref`) VALUES
  (1, 'product_1'),
  (2, 'product_2'),
  (3, 'product_3'),
  (4, 'product_4'),
  (5, 'product_5');

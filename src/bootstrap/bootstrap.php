<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Paneric\Local\Local;
use Paneric\ModuleResolver\DefinitionsCollector;
use Paneric\ModuleResolver\ModuleResolver;
use Slim\App;

try {
    $moduleResolverConfig = require './../src/config/module-resolver-config.php';
    $moduleMap = require './../src/config/module-map.php';
    $moduleResolver = new ModuleResolver();
    $processFoldersPaths = $moduleResolver->setProcessFoldersPaths(
        $_SERVER['REQUEST_URI'],
        $moduleResolverConfig,
        $moduleMap
    );

    // required by all routes.php
    $routePrefix = $moduleResolver->getRoutePrefix();

    $localValue = $moduleResolver->getLocal();
    $localConfig = require './../src/config/local-config.php';
    $local = new Local();
    $localValue = $local->setValue(
        $localConfig[ENV],
        $moduleResolverConfig['local_map'],
        $_COOKIE,
        $localValue
    );

    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        $processFoldersPaths,
        'config',
        $localConfig[ENV]['default_local'],
        ENV
    );

    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(false);
    $builder->addDefinitions($definitions);
    $container = $builder->build(); // Throws uncaught exception
    $container->set('local', $localValue);
    $slim = $container->get(App::class);
    $container->set('route_parser_interface', $slim->getRouteCollector()->getRouteParser());
    $container->set('api_url', $moduleResolver->getApiUrl());

    $fileNames = ['middleware.php', 'routes.php'];
    foreach ($processFoldersPaths as $path) {
        $folderPath = $path . 'bootstrap/';
        foreach ($fileNames as $fileName) {
            $filePath = $folderPath . $fileName;
            if (file_exists($filePath)) {
                require $filePath;
            }
        }
    }

    $slim->run();
} catch (Exception $e) {
    echo $e->getMessage();
}

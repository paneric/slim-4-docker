<?php

declare(strict_types=1);

use DI\DependencyException;
use DI\NotFoundException;
use Middlewares\Whoops;
use Paneric\Middleware\JwtAuthorizationMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Tuupola\Middleware\JwtAuthentication;

if (isset($slim, $container)) {
    try {
        //$slim->addMiddleware($container->get(JwtAuthorizationMiddleware::class));//always before UriMiddleware
        //$slim->addMiddleware($container->get(JwtAuthentication::class));//always before UriMiddleware
        $slim->addMiddleware($container->get(RouteMiddleware::class));//always before UriMiddleware
        $slim->addBodyParsingMiddleware();
        $slim->addRoutingMiddleware();
        $slim->add(Whoops::class);
        $slim->addErrorMiddleware(
            ENV === 'dev' || ENV === 'test', //false for PROD environment
            true,
            true,
            $loggerApp ?? null
        );
    } catch (DependencyException | NotFoundException | NotFoundExceptionInterface | ContainerExceptionInterface $e) {
        echo $e->getMessage();
    }
}

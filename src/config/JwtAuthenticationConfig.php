<?php

declare(strict_types=1);

namespace App\config;

class JwtAuthenticationConfig
{
    public function __invoke(): array
    {
        return [
            "attribute" => "jwt",
            "path" => ["/api"],
//            "ignore" => ["/api/token", "/admin/ping"],
            'secure' => false, // only for dev for prod and test env set true
//            "secure" => true,
//            "relaxed" => ["localhost", "dev.example.com"],
            'secret' => $_ENV['JWT_SECRET'],
            'algorithm' => ['HS512'],
//            "before" => function ($request, $arguments) {
//                return $request->withAttribute("test", "test");
//            },
//            "after" => function ($response, $arguments) {
//                return $response->withHeader("X-Brawndo", "plants crave");
//            },
            'error' => function ($response, $arguments) {
                $data['status'] = 401;
                $data['error'] = 'Unauthorized/' . $arguments['message'];
                return $response
                    ->withHeader('Content-Type', 'application/json;charset=utf-8')
                    ->getBody()->write(json_encode(
                        $data,
                        JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                    ));
            },
        ];
    }
}

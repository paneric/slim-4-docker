<?php

declare(strict_types=1);

namespace App\config;

class GuardConfig
{
    public function __invoke(): array
    {
        return [
            'random_string_length' => 128,
            'key_ascii' => $_ENV['KEY_ASCII'],
            'algo_password' => PASSWORD_BCRYPT,
            'options_algo_password' => [
                'cost' => 10,
            ],
            'algo_hash' => 'sha512',
            'algo_jwt_sign' => 'HS512',
            'unique_id_prefix' => '',
            'unique_id_more_entropy' => true,
            'open_ssl_args' => null,
        ];
    }
}

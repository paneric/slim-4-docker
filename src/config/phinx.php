<?php

return [
    'paths' => [
        'migrations' => 'src/Migrations',
        'seeds' => 'src/Migrations/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'migrations',
        'default_environment' => 'dev',
        'prod' => [
            'adapter' => 'mysql',
            'host' => 'slim-4-docker-mysql',
            'name' => 'slim-4-docker',
            'user' => 'toor',
            'pass' => 'toor',
            'port' => '3306',
            'charset' => 'utf8',
        ],
        'dev' => [
            'adapter' => 'mysql',
            'host' => 'slim-4-docker-mysql',
            'name' => 'slim-4-docker',
            'user' => 'toor',
            'pass' => 'toor',
            'port' => '3306',
            'charset' => 'utf8',
        ],
        'test' => [
            'adapter' => 'mysql',
            'host' => 'slim-4-docker-mysql',
            'name' => 'slim-4-docker',
            'user' => 'toor',
            'pass' => 'toor',
            'port' => '3306',
            'charset' => 'utf8',
        ]
    ],
    'version_order' => 'creation'
];

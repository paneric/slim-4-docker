<?php

declare(strict_types=1);

return [
    'default_route_key' => 'main',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'route_prefix' => ['api', 'app'],// 'api' for api app
    'merge_module_cross_map' => false,
    'app_path'  => './../src/',// or './../Auth/' in case of Auth microservice
    'root_paths'  => ['./../'],// or package, for example './../vendor/paneric/Package/'
];

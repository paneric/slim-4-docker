<?php

declare(strict_types=1);

use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;

return [
    'root_folder' => ROOT_FOLDER,

    'base_uri' => '',

    'main_route_name' => '',

    'dbal' => [
        'limit' => 10,
        'host' => $_ENV['DB_HOST'],
        'charset' => 'utf8',
        'dbName' => $_ENV['DB_NAME'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PSWD'],
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::MYSQL_ATTR_FOUND_ROWS => true//count rowCounts even if identical values updated
        ],
    ],

    'pagination-middleware' => [
        'actions.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'action-privileges.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'credentials.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'privileges.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'roles.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'users.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'page_rows_number' => 20,//middleware
    ],
];

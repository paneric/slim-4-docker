<?php

declare(strict_types=1);

return [
    'dbal' => [
        'limit' => 10,
        'host' => $_ENV['DB_HOST'],
        'charset' => 'utf8',
        'dbName' => $_ENV['DB_NAME'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PSWD'],
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::MYSQL_ATTR_FOUND_ROWS => true//count rowCounts even if identical values updated
        ],
    ],

    'migrations' => [
        'folder_paths' => [
            'src/Migrations/' => 'App\\Migrations',
            'Auth/Migrations/' => 'Paneric\\Auth\\Migrations',
            'Commerce/Migrations/' => 'Paneric\\Commerce\\Migrations',
            'PIM/Migrations/' => 'Paneric\\PIM\\Migrations',
        ],
    ],
];

<?php

declare(strict_types=1);

return array_merge(
    require './../Auth/config/module-map.php',
    ['main' => './../src'],
);

<?php

declare(strict_types=1);

use App\config\JwtAuthenticationConfig;
use Paneric\Middleware\JwtAuthorizationMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Container\ContainerInterface;
use Tuupola\Middleware\JwtAuthentication;

return [
    RouteMiddleware::class => static function (ContainerInterface $container): RouteMiddleware {
        return new RouteMiddleware($container);
    },
    PaginationMiddleware::class => static function (ContainerInterface $container): PaginationMiddleware {
        return new PaginationMiddleware(
            $container
        );
    },
    JwtAuthentication::class => static function (ContainerInterface $container): JwtAuthentication {
        $jwtAuthenticationConfig = $container->get(JwtAuthenticationConfig::class);

        return new JwtAuthentication(
            $jwtAuthenticationConfig(),
        );
    },
    JwtAuthorizationMiddleware::class => static function (ContainerInterface $container): JwtAuthorizationMiddleware {
        $jwtAuthenticationConfig = $container->get(JwtAuthenticationConfig::class);

        return new JwtAuthorizationMiddleware(
            $jwtAuthenticationConfig(),
        );
    },
];

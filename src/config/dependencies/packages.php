<?php

declare(strict_types=1);

use App\config\GuardConfig;
use App\Migrations\Infrastructure\MigrationRepository;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;
use Paneric\ComponentModule\Adapter\Guard;
use Paneric\ComponentModule\Interfaces\GuardInterface;
use Paneric\Logger\EventLogger;
use Paneric\Logger\HttpClientLogger;
use Paneric\Migrations\Command\Config;
use Paneric\Migrations\MigrationRepositoryInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use RandomLib\Factory as RandomFactory;

return [
    Manager::class => static function (ContainerInterface $container): Manager {
        return new Manager(
            (new PDOBuilder())->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },

    GuardInterface::class => static function (ContainerInterface $c): Guard {
        $randomFactory = new RandomFactory();
        return new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            $c->get(GuardConfig::class)
        );
    },

    Config::class => static function (ContainerInterface $container): Config {
        return new Config(
            $container->get('migrations'),
        );
    },

    MigrationRepositoryInterface::class => static function (
        ContainerInterface $container
    ): MigrationRepositoryInterface {
        $migrationRepository = new MigrationRepository($container->get(Manager::class));
        $migrationRepository->createTable();
        return $migrationRepository;
    },

    EventLogger::class => static function (
        ContainerInterface $container
    ): LoggerInterface {
        $logger = new EventLogger('event_logger');
        $streamHandler = new StreamHandler('./../var/log/event-logger.log', Level::Debug);
        $streamHandler->setFormatter(
            new LineFormatter(null, null, false, true)
        );
        $logger->pushHandler($streamHandler);
        return $logger;
    },

    HttpClientLogger::class => static function (
        ContainerInterface $container
    ): HttpClientLogger {
        $logger = new HttpClientLogger('http_client_logger');
        $streamHandler = new StreamHandler('./../var/log/http-client-logger.log', Level::Debug);
        $streamHandler->setFormatter(
            new LineFormatter(null, null, false, true)
        );
        $logger->pushHandler($streamHandler);
        return $logger;
    },
];

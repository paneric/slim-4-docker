<?php

declare(strict_types=1);

namespace App\Migrations\Infrastructure;

use App\Migrations\Model\MigrationDAO;
use Exception;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;
use Paneric\Migrations\MigrationRepositoryInterface;
use PDO;

class MigrationRepository extends Repository implements MigrationRepositoryInterface
{
    private const SQL_PATH_UP = 'vendor/paneric/migrations/script/migration-up.sql';

    /**
     * @throws Exception
     */
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'migration';
        $this->daoClass = MigrationDAO::class;
        $this->fetchMode = PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE;

        $this->adaptManager();
    }

    /**
     * @throws Exception
     */
    public function createTable(): void
    {
        $stmt = $this->manager->query(sprintf("SHOW TABLES LIKE '%s'", $this->table));

        if ($stmt->rowCount() === 1) {
            return;
        }

        $this->manager->multiQuery(file_get_contents(self::SQL_PATH_UP));

        $stmt = $this->manager->query(sprintf("SHOW TABLES LIKE '%s'", $this->table));

        if ($stmt->rowCount() === 0) {
            throw new Exception(
                sprintf('Creating table "%s" failure', $this->table)
            );
        }
    }

    public function findOneByRef(string $ref): mixed
    {
        return $this->findOneBy(['mig_ref' => $ref]);
    }

    /**
     * @throws Exception
     */
    public function execute(string $multiSql, array $migration): void
    {
        $migration = [
            'mig_ref' => $migration['ref'],
            'mig_description' => $migration['description'],
        ];

        $this->manager->multiQuery($multiSql);
        $this->adaptManager();

        if (!$this->manager->create($migration)) {
            throw new Exception(sprintf(
                'Migration "%s" is executed properly, but "migration" table is not updated.',
                $migration['mig_ref']
            ));
        }
    }
}

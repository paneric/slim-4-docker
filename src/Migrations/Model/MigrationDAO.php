<?php

declare(strict_types=1);

namespace App\Migrations\Model;

use Paneric\DataObject\DAO;

class MigrationDAO extends DAO
{
    protected ?int $id;
    protected string $ref = '';
    protected string $description;

    public function __construct()
    {
        $this->prefix = 'mig_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
